package net.jackcroft.database;

import java.sql.SQLException;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author jack
 *
 */
public class DatabaseTest {
	
	private static Logger logger = LoggerFactory.getLogger(DatabaseTest.class);
	private DateTimeFormatter isoTime=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZZ");
	private Database db;
	
	@BeforeClass
	 public void setUp() {
		db=new Database();
	 }
	
	@Test
	public void testUser()
	{
		db.addUser("test", "testing");
		Assert.assertEquals(true,db.userExists("test"));
		Assert.assertEquals("testing", db.findUserPassword("test"));
		db.deleteUser("test");
		Assert.assertEquals(false,db.userExists("test"));
	}
	
	@Test
	public void testUpdatePassword()
	{
		db.addUser("test", "testing");
		db.updatePassword("test", "notTesting");
		Assert.assertEquals("notTesting",db.findUserPassword("test"));
		db.deleteUser("test");
	}
	
	@Test
	public void testRoom()
	{
		db.addUser("test", "testing");
		try 
		{
			db.createRoom("testRoom", true, "test");
		} 
		catch (SQLException e) 
		{	
			logger.error("Test Room Error: "+e.getMessage());
			Assert.assertFalse(true);
		}
		Assert.assertEquals(true, db.roomIsPublic("testRoom"));
		Assert.assertEquals(true,db.roomExists("testRoom"));
		db.deleteRoom("testRoom");
		Assert.assertEquals(false, db.roomExists("testRoom"));
		db.deleteUser("test");
	}
	
	@Test
	public void testChatRooms()
	{
		db.addUser("test", "testing");
		db.addUser("cool", "beans");
		try 
		{
			db.createRoom("testRoom", true, "test");
			db.createRoom("coolRoom",false,"test");
		} 
		catch (SQLException e) 
		{
			logger.error("Test Chat Rooms Error: "+e.getMessage());
			Assert.assertFalse(true);
		}
		
		db.addUserToRoom("coolRoom", "cool");
		ArrayList<String> chatRooms=db.getRoomsForUser("cool");
		Assert.assertEquals(true, chatRooms.contains("testRoom"));
		Assert.assertEquals(true, chatRooms.contains("coolRoom"));
		chatRooms=db.getRoomsForOwner("test");
		Assert.assertEquals(true, chatRooms.contains("testRoom"));
		Assert.assertEquals(true, chatRooms.contains("coolRoom"));
		chatRooms=db.getPrivateRoomsForOwner("test");
		Assert.assertEquals(true, chatRooms.contains("coolRoom"));
		db.deleteRoom("testRoom");
		db.deleteRoom("coolRoom");
		Assert.assertEquals(false, db.roomExists("testRoom"));
		db.deleteUser("test");
		db.deleteUser("cool");
	}
	
	@Test
	public void testMessages()
	{
		db.addUser("test", "testing");
		try 
		{
			db.createRoom("testRoom2", true, "test");
		} 
		catch (SQLException e) 
		{
			logger.error("Test Chat Rooms Error: "+e.getMessage());
			Assert.assertFalse(true);
		}
		DateTime dt=new DateTime();
		db.addMessage("test", "testRoom2", "test message", isoTime.print(dt));
		ArrayList<String>[] messages=db.getMessages("testRoom2");
		Assert.assertEquals(messages[1].get(0), "test message");
		db.deleteRoom("testRoom2");
		db.deleteUser("test");
	}
	
	@AfterClass
	public void tearDown()
	{
		db.deleteRoom("testRoom");
		db.deleteRoom("coolRoom");
		db.deleteRoom("testRoom2");
		db.deleteUser("test");
		db.deleteUser("cool");
		db.close();
	}
}
