/**
 * 
 */
package net.jackcroft.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.jackcroft.chat.ChatServer;
import net.jackcroft.util.BaseEncoding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class CryptoHashing {

	/**
	 * hashes a given password with a given salt
	 */
	private static Logger logger = LoggerFactory.getLogger(ChatServer.class);
	
	public static String hashPassword(String salt, String password)
	{
		byte[] bytes=null;
		String hex="";
		
		String concat=salt+password;
		MessageDigest md;
		
		try 
		{
			md = MessageDigest.getInstance("SHA-256");
			bytes=md.digest(concat.getBytes());
			for(int i=0;i<1000;i++)
			{
				bytes=md.digest(bytes);
			}
			hex=BaseEncoding.convertToHex(bytes);
		} 
		catch (NoSuchAlgorithmException e) 
		{
			logger.error("Hashing Error: "+e.getMessage());
		}
		
		return hex;
	}
}
