package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.DeleteView;
import net.jackcroft.crypto.CryptoHashing;

public class DeleteController implements Controller{

	/**
	 * controls the view for deleting a user
	 */
	
	private Messenger messenger;
	private DeleteView view;
	private String username;
	private String password;
	private MainController main;
	
	public DeleteController(DeleteView v, Messenger m)
	{
		messenger=m;
		view=v;
		v.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.DATAREADY)	//deletes a user
		{
			username=view.getUser().getUserId();
			password=view.getUser().getPassword();
			password=CryptoHashing.hashPassword(username,password);
			messenger.send("delete", this);
			messenger.send(username, this);
			messenger.send(password, this);
			view.deleted();
		}
		if(type==ControllerMessageType.CANCEL)	//closes the view
		{
			view.destroy();
			main.update(ControllerMessageType.CANCEL);
		}
	}
	
	//handles whether or not the account got deleted
	public void response(String reply)
	{
		if(reply.equals("account.deleted"))
		{
			main.update(ControllerMessageType.DELETEDONE);
			view.destroy();
		}
		if(reply.equals("delete.failed"))
		{
			view.error("Incorrect Username or Password");
		}
	}
	
	//shows the window
	public void start()
	{
		view.show();
	}
	
	//sets the main controller
	public void setMainController(MainController mc)
	{
		main=mc;
	}
}
