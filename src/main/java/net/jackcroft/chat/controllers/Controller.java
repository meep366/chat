package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ControllerMessageType;

public interface Controller {	//basic interface for all controllers

	public void update(ControllerMessageType type);
	public void response(String reply);
	public void start();
}
