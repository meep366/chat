/**
 * 
 */
package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.ChatRoomAddView;

/**
 * @author jack
 *
 */
public class ChatRoomAddController implements Controller{

	
	/**
	 * controls the window for creating a new chat room
	 */
	private ChatRoomAddView view;
	private MainController main;
	private Messenger messenger;
	private String isPublic="false";
	private String roomName;
	
	public ChatRoomAddController(ChatRoomAddView crav, Messenger m)
	{
		messenger=m;
		view=crav;
		view.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.DATAREADY)	//creates a new room
		{
			roomName=view.getName();
			if(view.isPublic())
				isPublic="true";
			
			messenger.send("add.room", this);
			messenger.send(roomName, this);
			messenger.send(ChatState.getUser().getUserId(), this);
			messenger.send(isPublic, this);
			view.added();
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
			view.destroy();
	}
	
	//controls adding room or displaying the error
	public void response(String reply)
	{
		if(reply.equals("room.added"))
		{
			view.destroy();
			main.update(ControllerMessageType.ROOMADDED);
		}
		if(reply.equals("room.taken"))
			view.error("Room Already Taken");
	}
	
	//returns the room
	public String getRoom()
	{
		return roomName;
	}
	
	//opens the window
	public void start()
	{
		view.show();
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
}
