package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.User;
import net.jackcroft.chat.views.EditView;
import net.jackcroft.crypto.CryptoHashing;

public class EditController implements Controller{

	/**
	 * controls the view for changing the password
	 */
	
	private Messenger messenger;
	private EditView view;
	private String username;
	private String oldPassword;
	private String newPassword;
	private MainController main;
	
	public EditController(EditView v, Messenger m)
	{
		messenger=m;
		view=v;
		v.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.LOGIN)		//makes sure the username and password are correct
		{
			username=view.getUser();
			oldPassword=view.getOldPassword();
			oldPassword=CryptoHashing.hashPassword(username,oldPassword);
			messenger.send("login", this);
			messenger.send(username, this);
			messenger.send(oldPassword, this);
			view.edited();
		}
		if(type==ControllerMessageType.CHANGEPASSWORD)	//changes the password to the new password
		{
			newPassword=view.getNewPassword();
			newPassword=CryptoHashing.hashPassword(username,newPassword);
			messenger.send("edit", this);
			messenger.send(username, this);
			messenger.send(newPassword, this);
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
		{
			view.destroy();
			main.update(ControllerMessageType.CANCEL);
		}
	}
	
	//handles checking old password and changing new password
	public void response(String reply)
	{
		if(reply.equals("login.accepted"))
			view.change();
		if(reply.equals("login.failed"))
		{
			view.edited();
			view.error("Incorrect Username or Password");
		}
		if(reply.equals("password.changed"))
		{
			view.destroy();
			ChatState.setUser(new User(username,newPassword));
			main.update(ControllerMessageType.DONE);
		}
	}
	
	//shows the view
	public void start()
	{
		view.show();
	}
	
	//sets the main controller
	public void setMainController(MainController mc)
	{
		main=mc;
	}
}
