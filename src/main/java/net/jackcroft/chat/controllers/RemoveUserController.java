package net.jackcroft.chat.controllers;

import java.util.ArrayList;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.RemoveUserView;

public class RemoveUserController implements Controller{

	/**
	 * controls the view for removing a user from a chat room
	 */
	
	private MainController main;
	private Messenger messenger;
	private RemoveUserView view;
	private String user;
	private String room;
	private ArrayList<String> rooms=new ArrayList<String>();
	private boolean addingRooms=false;
	
	public RemoveUserController(RemoveUserView v, Messenger m)
	{
		messenger=m;
		view=v;
		v.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.REMOVEUSER)	//removes the user from the room
		{
			user=view.getUser();
			room=view.getRoom();
			if(room!=null&&user!=null)
			{
				messenger.send("remove.user", this);
				messenger.send(user, this);
				messenger.send(room, this);
			}
			else
				view.error("Empty Field");
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
		{
			view.destroy();
		}
	}
	
	//handles adding rooms and removing user
	public void response(String reply)
	{
		if(addingRooms)
		{
			if(reply.equals("end.rooms"))
			{
				view.show();
				addingRooms=false;
			}
			else
				rooms.add(reply);
		}
		else
		{
			if(reply.equals("user.removed"))
				view.destroy();
			if(reply.equals("no.such.user"))
				view.error("No Such User");
		}
	}
	
	//sends request for rooms
	public void start()
	{
		addingRooms=true;
		rooms=new ArrayList<String>();
		messenger.send("get.private.owner.rooms", this);
		messenger.send(ChatState.getUser().getUserId(), this);
	}
	
	//converts arraylist to string[]
	public String[] getRooms()
	{
		String[] result=new String[rooms.size()];
		for(int i=0;i<rooms.size();i++)
		{
			result[i]=rooms.get(i);
		}
		return result;
	}
	
	//sorts a given string[]
	public String[] sort(String[] list)
	{
		return main.sort(list);
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
}
