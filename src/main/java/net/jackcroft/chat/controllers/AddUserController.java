/**
 * 
 */
package net.jackcroft.chat.controllers;

import java.util.ArrayList;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.AddUserView;

/**
 * @author jack
 *
 */
public class AddUserController implements Controller{

	/**
	 * controller for adding a user to a chat room
	 */
	
	private AddUserView view;
	private Messenger messenger;
	private MainController main;
	private String user;
	private String room;
	private ArrayList<String> rooms=new ArrayList<String>();
	private boolean addingRooms=false;
	
	public AddUserController(AddUserView v, Messenger m)
	{
		view=v;
		messenger=m;
		view.setController(this);
	}

	public void update(ControllerMessageType type) 
	{
		if(type==ControllerMessageType.ADDUSER)		//adding a user
		{
			user=view.getUser();
			room=view.getRoom();
			if(room!=null&&user!=null)
			{
				messenger.send("add.user", this);
				messenger.send(user, this);
				messenger.send(room, this);
			}
			else
				view.error("Empty Field");
			view.clicked();
		}
		if(type==ControllerMessageType.CANCEL)		//closing the window
			view.destroy();
	}
	
	//controls adding rooms and seeing if the user was added
	public void response(String reply) 
	{
		if(addingRooms)
		{
			if(reply.equals("end.rooms"))
			{
				view.show();
				addingRooms=false;
			}
			else
				rooms.add(reply);
		}
		else
		{
			if(reply.equals("user.added"))
				view.destroy();
			if(reply.equals("no.such.user"))
				view.error("No Such User");
		}
	}
	
	//sends out request for list of rooms
	public void start() 
	{
		addingRooms=true;
		rooms=new ArrayList<String>();
		messenger.send("get.private.owner.rooms", this);
		messenger.send(ChatState.getUser().getUserId(), this);
	}
	
	//converts the arraylist into an array
	public String[] getRooms()
	{
		String[] result=new String[rooms.size()];
		for(int i=0;i<rooms.size();i++)
			result[i]=rooms.get(i);
		return result;
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
	
	//sorts a given list
	public String[] sort(String[] list)
	{
		return main.sort(list);
	}
}
