package net.jackcroft.chat.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.ChatView;
import net.jackcroft.chat.views.MainView;
import net.jackcroft.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class MainController implements Controller{

	/**
	 * controls all the other controllers and main window
	 */
	
	private static Logger logger = LoggerFactory.getLogger(MainController.class);
	private MainView view;
	private Messenger messenger;
	private LoginController login;
	private RegisterController register;
	private ArrayList<ChatController> chats;
	private DeleteController delete;
	private EditController edit;
	private ChatRoomsController chatRooms;
	private ChatRoomAddController chatAdd;
	private AddUserController addUser;
	private RemoveUserController removeUser;
	private ChangePrivacyController changePrivacy;
	private DeleteRoomController deleteRoom;
	private BadOpeningController badOpening;
	private boolean loggedIn=false;
	
	public MainController(MainView v, Messenger m, LoginController lc, RegisterController rc, DeleteController dc, EditController ec, ChatRoomsController crc, ChatRoomAddController crac, AddUserController auc, RemoveUserController ruc, ChangePrivacyController cpc, DeleteRoomController drc, BadOpeningController boc)
	{
		login=lc;
		register=rc;
		chats=new ArrayList<ChatController>();
		view=v;
		messenger=m;
		delete=dc;
		edit=ec;
		chatRooms=crc;
		chatAdd=crac;
		addUser=auc;
		removeUser=ruc;
		changePrivacy=cpc;
		deleteRoom=drc;
		badOpening=boc;
		v.setController(this);
	}
	
	//shows the view
	public void start()
	{
		view.show();
	}
	
	public void update(ControllerMessageType type)
	{
		logger.debug("Main Update: "+type);
		
		if(type==ControllerMessageType.LOGIN)	//logs a user in
		{
			login.start();
			view.disableButtons();
		}
		if(type==ControllerMessageType.REGISTER)	//registers a new user
		{
			register.start();
			view.disableButtons();
		}
		if(type==ControllerMessageType.DELETEACCOUNT)	//deletes a user
		{
			delete.start();
		}
		if(type==ControllerMessageType.CHANGEPASSWORD)	//changes a user password
		{
			edit.start();
		}
		if(type==ControllerMessageType.DELETEDONE)	//delete user was successful
		{
			if(loggedIn)
				view.show();
			view.enableButtons();
			loggedIn=false;
		}
		if(type==ControllerMessageType.DONE)	//login or register was successful
		{
			chatRooms.start();
			loggedIn=true;
			view.destroy();
		}
		if(type==ControllerMessageType.ROOMSELECTED)	//open up new chat window
		{
			ChatView cv=new ChatView();
			ChatController cc=new ChatController(cv,messenger);
			chats.add(cc);
			cc.setChatRoom(chatRooms.getSelected());
			messenger.addController(cc);
			cc.start();
			cc.setMainController(this);
		}
		if(type==ControllerMessageType.ADDROOM)	//create a new chat room
		{
			chatAdd.start();
		}
		if(type==ControllerMessageType.CANCEL)	//cancel was pressed on logging in or editing user
		{
			if(!loggedIn)
				view.enableButtons();
			else
				chatRooms.start();
		}
		if(type==ControllerMessageType.ROOMADDED)	//new chat room was created
		{
			ChatView cv=new ChatView();
			ChatController cc=new ChatController(cv,messenger);
			chats.add(cc);
			cc.setChatRoom(chatAdd.getRoom());
			messenger.addController(cc);
			cc.start();
			cc.setMainController(this);
		}
		if(type==ControllerMessageType.REFRESH)
		{
			chatRooms.refresh();
		}
		if(type==ControllerMessageType.LOGOUT)	//logging out from chat rooms
		{
			if(loggedIn)	
			{
				view.show();
				view.enableButtons();
			}
			loggedIn=false;
		}
		if(type==ControllerMessageType.ADDUSER)	//adding a user to a chat room
			addUser.start();
		if(type==ControllerMessageType.REMOVEUSER)	//removing a user from a chat room
			removeUser.start();
		if(type==ControllerMessageType.CHANGEPRIVACY)	//changing the privacy on a chat room
			changePrivacy.start();
		if(type==ControllerMessageType.DELETEROOM)	//deleting a chat room
			deleteRoom.start();
		if(type==ControllerMessageType.DELETEROOMDONE)	//deleting room was successful
			chatRooms.refresh();
		if(type==ControllerMessageType.CLOSE)
			messenger.send(".bye", this);
		if(type==ControllerMessageType.CHATCLOSE)
		{
			for(int i=0;i<chats.size();i++)
			{
				if(chats.get(i).isClosed())
				{
					messenger.remove(chats.get(i));
					chats.remove(i);
					i--;
				}
			}
		}
	}
	
	//sorts a given arraylist
	public ArrayList<String> sort(ArrayList<String> list)
	{
		TreeSet<String> sorted=new TreeSet<String>();
		sorted.addAll(list);
		list=new ArrayList<String>();
		list.addAll(sorted);
		return list;
	}
	
	//sorts a given String[]
	public String[] sort(String[] list)
	{
		TreeSet<String> sorted=new TreeSet<String>();
		for(int i=0;i<list.length;i++)
			sorted.add(list[i]);
		
		Iterator<String> i=sorted.iterator();
		
		for(int j=0;i.hasNext();j++)
			list[j]=i.next();
		
		return list;
	}
	
	public void response(String reply)
	{
		if(reply.equals("connected"))
			start();
		else if(reply.equals("no.connection"))
			badOpening.start();
			
	}
	public static void main(String[] args)
	{
		Database db=new Database();
	//	db.addUser("test", "testing");
		db.deleteUser("test");
	}
}
