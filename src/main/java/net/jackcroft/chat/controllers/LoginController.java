/**
 * 
 */
package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.User;
import net.jackcroft.chat.views.LoginView;
import net.jackcroft.crypto.CryptoHashing;

/**
 * @author jack
 *
 */
public class LoginController implements Controller{

	/**
	 * controls the window for logging in
	 */
	
	private LoginView view;
	private Messenger messenger;
	private String username;
	private String password;
	private MainController main;
	private boolean loggedIn=false;
	
	public LoginController(LoginView lv, Messenger m)
	{
		view=lv;
		lv.setController(this);
		messenger=m;
	}
	
	//shows the view
	public void start()
	{
		view.show();
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.DATAREADY)	//logs someone in
		{
			username=view.getUser().getUserId();
			password=view.getUser().getPassword();
			password=CryptoHashing.hashPassword(username,password);
			messenger.send("login", this);
			messenger.send(username, this);
			messenger.send(password, this);
			view.loggedIn();
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
		{
			if(!loggedIn)
				main.update(ControllerMessageType.CANCEL);
			
			view.destroy();
			loggedIn=false;
		}
		
	}
	
	//handles logging in success or failure
	public void response(String reply)
	{
		if(reply.equals("login.accepted"))
		{
			loggedIn=true;
			ChatState.setUser(new User(username,password));
			view.destroy();
			main.update(ControllerMessageType.DONE);
		}
		if(reply.equals("login.failed"))
		{
			view.error("Incorrect Username or Password");
		}
	}
	
	//sets the main controller
	public void setMainController(MainController mc)
	{
		main=mc;
	}
}
