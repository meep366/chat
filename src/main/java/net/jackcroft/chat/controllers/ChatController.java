package net.jackcroft.chat.controllers;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.ChatView;

public class ChatController implements Controller{

	/**
	 * controls the window for chatting
	 */
	
	private ChatView view;
	private Messenger messenger;
	private MainController main;
	private String chatRoom;
	private boolean closed=false;
	private DateTimeFormatter longTime=DateTimeFormat.forPattern("dd/MM/yy HH:mm");
	private DateTimeFormatter shortTime=DateTimeFormat.forPattern("HH:mm");
	private DateTimeFormatter isoTime=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSZZ");
	private int message=0;
	private String messageText;
	private String user;
	private int group=0;
	private ArrayList<String>[] oldMessages=new ArrayList[3];
	
	
	public ChatController(ChatView v, Messenger m)
	{
		view=v;
		messenger=m;
		v.setController(this);
		for(int i=0;i<oldMessages.length;i++)
		{
			oldMessages[i]=new ArrayList<String>();
		}
	}
	
	//starts the window
	public void start()
	{
		messenger.send("get.messages", this);
		messenger.send(chatRoom, this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.SEND)	//sends a message out
		{
			messenger.send("message", this);
			messenger.send(chatRoom, this);
			messenger.send(ChatState.getUser().getUserId(),this);
			messenger.send(view.getText(),this);
			messenger.send(isoTime.print(view.getTime()), this);
		}
		if(type==ControllerMessageType.QUIT)	//closes the window
		{
			closed=true;
			main.update(ControllerMessageType.CHATCLOSE);
			view.destroy();
		}
	}
	
	public boolean isClosed()
	{
		return closed;
	}
	
	//prints out messages from the Messenger
	public void response(String reply)
	{
		if(message==0&&group==0)
		{
			if(reply.equals("message"))
				message=1;
			else if(reply.equals("no.messages"))
			{
				main.update(ControllerMessageType.REFRESH);
				view.show();
			}
			else if(!reply.equals("end.rooms"))
			{
				oldMessages[0].add(reply);
				group=1;
			}
		}
		else if(message>0&&group==0)
		{
			if(message==1)
			{
				user=reply+":";
				message=2;
			}
			else if(message==2)
			{
				messageText=reply;
				message=3;
			}
			else if(message==3)
			{	
				DateTime oneDay=new DateTime().minusDays(1);
				
				if(DateTime.parse(reply,isoTime).isBefore(oneDay))
					view.print(longTime.print(DateTime.parse(reply,isoTime)), user, messageText);
				else
					view.print(shortTime.print(DateTime.parse(reply,isoTime)), user, messageText);
				
				message=0;
			}
		}
		else if(message==0&&group>0)
		{
			if(group==1)
			{
				if(reply.equals("end.group"))
					group=2;
				else
					oldMessages[0].add(reply);
			}
			else if(group==2)
			{
				if(reply.equals("end.group"))
					group=3;
				else
					oldMessages[1].add(reply);
			}
			else if(group==3)
			{
				if(reply.equals("end.group"))
				{
					group=0;
					view.show();
					main.update(ControllerMessageType.REFRESH);
					for(int i=0;i<oldMessages[1].size();i++)
					{
						view.print(oldMessages[2].get(i),oldMessages[0].get(i)+":", oldMessages[1].get(i));
					}
				}
				else
				{
					DateTime oneDay=new DateTime().minusDays(1);
					if(DateTime.parse(reply,isoTime).isBefore(oneDay))
						oldMessages[2].add(longTime.print(DateTime.parse(reply,isoTime)));
					else
						oldMessages[2].add(shortTime.print(DateTime.parse(reply,isoTime)));
				}
			}
		}
	}
	
	//sets the main controller
	public void setMainController(MainController mc)
	{
		main=mc;
	}
	
	//gets the chat room that is in this window
	public String getChatRoom() 
	{
		return chatRoom;
	}

	//sets the chat room of the window
	public void setChatRoom(String chat) 
	{
		chatRoom = chat;
	}
	
	public String toString()
	{
		return chatRoom;
	}

}
