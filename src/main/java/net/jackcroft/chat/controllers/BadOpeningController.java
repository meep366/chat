/**
 * 
 */
package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.BadOpeningView;

/**
 * @author jack
 *
 */
public class BadOpeningController implements Controller{

	/**
	 * Controller for bad opening
	 */
	
	private BadOpeningView view;
	private MainController main;
	private Messenger messenger;
	
	public BadOpeningController(BadOpeningView v, Messenger m)
	{
		view=v;
		messenger=m;
		v.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		
	}
	
	public void response(String reply)
	{
		
	}
	
	public void start()
	{
		view.show();
	}
}
