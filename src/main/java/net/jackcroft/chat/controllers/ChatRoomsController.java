package net.jackcroft.chat.controllers;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.ChatRoomsView;

public class ChatRoomsController implements Controller{
	
	/**
	 * controls the chat rooms view
	 */
	
	//Check up on .stop() method
	//Make chat room startup faster
	//Back things up on Amazon
	//New sql statements
	//Liquibase
	//Find out why displaying room label takes long time
	//Fix logout of chat rooms issue
	
	private static Logger logger = LoggerFactory.getLogger(ChatRoomsController.class);
	private ChatRoomsView view;
	private MainController main;
	private Messenger messenger;
	private String selected;
	private ArrayList<String> rooms=new ArrayList<String>();
	private boolean refresh=false;
	private boolean open=false;
	
	public ChatRoomsController(ChatRoomsView v, Messenger m)
	{
		view=v;
		messenger=m;
		v.setController(this);
	}
	
	//returns the list of rooms
	public ArrayList<String> getRooms()
	{
		return rooms;
	}
	
	public void update(ControllerMessageType type)
	{
		logger.debug("Chat Room Update: "+type);
		if(type==ControllerMessageType.DATAREADY)	//opens up a new chat view
		{
			selected=view.getSelected();
			main.update(ControllerMessageType.ROOMSELECTED);
			view.refresh();
			view.entered();
		}
		if(type==ControllerMessageType.ADDROOM)	//opens the add room view
		{
			main.update(ControllerMessageType.ADDROOM);
			view.refresh();
		}
		if(type==ControllerMessageType.LOGOUT)	//returns to the main menu
		{
			main.update(ControllerMessageType.LOGOUT);
			open=false;
			refresh=false;
			view.destroy();
		}
		if(type==ControllerMessageType.CHANGEPASSWORD)	//opens up the change password view
		{
			main.update(ControllerMessageType.CHANGEPASSWORD);
			view.destroy();
			open=false;
		}
		if(type==ControllerMessageType.DELETEACCOUNT)	//opens up the delete account view
		{
			main.update(ControllerMessageType.DELETEACCOUNT);
			view.destroy();
			open=false;
			refresh=false;
		}
		if(type==ControllerMessageType.ADDUSER)	//opens the add user to chat room view
			main.update(ControllerMessageType.ADDUSER);
		if(type==ControllerMessageType.REMOVEUSER)	//opens the remove user from chat room view
			main.update(ControllerMessageType.REMOVEUSER);
		if(type==ControllerMessageType.CHANGEPRIVACY)	//opens up the change room privacy view
			main.update(ControllerMessageType.CHANGEPRIVACY);
		if(type==ControllerMessageType.DELETEROOM)	//opens up the delete chat room view
			main.update(ControllerMessageType.DELETEROOM);
		if(type==ControllerMessageType.CLOSE)
		{
			messenger.send(".bye", this);
		}
	}
	
	//sorts a given arraylist
	public ArrayList<String> sort(ArrayList<String> list)
	{
		return main.sort(list);
	}
	
	//controls logging in, refreshing and getting rooms
	public void response(String reply)
	{
		if(!reply.equals("end.rooms"))
		{
			if(!rooms.contains(reply))
			{
				rooms.add(reply);
			}
		}
		else
		{
			if(!refresh&&!open)
			{
				view.show();
				open=true;
			}
			if(refresh)
				view.refresh();
		}
	}
	
	//refreshes the list of rooms
	public void refresh()
	{
		refresh=true;
		start();
	}
	
	//opens up the window
	public void start()
	{
		rooms=new ArrayList<String>();
		messenger.send("get.rooms", this);
		messenger.send(ChatState.getUser().getUserId(), this);
	}
	
	//returns the select room
	public String getSelected()
	{
		return selected;
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
}
