package net.jackcroft.chat.controllers;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.User;
import net.jackcroft.chat.views.RegisterView;
import net.jackcroft.crypto.CryptoHashing;


public class RegisterController implements Controller{

	/**
	 * controls the view to register a new user
	 */
	
	private String user;
	private String password;
	private RegisterView view;
	private Messenger messenger;
	private MainController main;
	private boolean registered=false;
	
	public RegisterController(RegisterView rw, Messenger m)
	{
		rw.setController(this);
		messenger=m;
		view=rw;
	}
	
	//shows the view
	public void start()
	{
		view.show();
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.DATAREADY)	//registers a new user
		{
			user=view.getUser();
			password=view.getPassword();
			password=CryptoHashing.hashPassword(user,password);
			messenger.send("register",this);
			messenger.send(user,this);
			messenger.send(password,this);
			view.registered();
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
		{
			if(!registered)
				main.update(ControllerMessageType.CANCEL);
			
			 view.destroy();
			 registered=false;
		}
	}
	
	//handles user adding success or failure
	public void response(String reply)
	{
		if(reply.equals("user.added"))
		{
			registered=true;
			view.destroy();
			ChatState.setUser(new User(user,password));
			main.update(ControllerMessageType.DONE);
		}
		if(reply.equals("user.taken"))
			view.error("Username already taken");
	}
	
	//sets the main controller
	public void setMainController(MainController mc)
	{
		main=mc;
	}
}
