/**
 * 
 */
package net.jackcroft.chat.controllers;

import java.util.ArrayList;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.ChangePrivacyView;

/**
 * @author jack
 *
 */
public class ChangePrivacyController implements Controller{

	/**
	 * 	controller for changing the privacy of a chat room
	 */
	
	private MainController main;
	private ChangePrivacyView view;
	private Messenger messenger;
	private String room;
	private ArrayList<String> rooms=new ArrayList<String>();
	private boolean addingRooms=false;
	
	public ChangePrivacyController(ChangePrivacyView v, Messenger m)
	{
		view=v;
		messenger=m;
		view.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.CHANGEPRIVACY)	//changing the privacy
		{
			room=view.getRoom();
			if(room!=null)
			{
				messenger.send("change.privacy", this);
				messenger.send(room, this);
			}
			else
				view.error("No Room Selected");
		}
		if(type==ControllerMessageType.CANCEL)	//closing the window
			view.destroy();
		
		if(type==ControllerMessageType.GETPRIVACY)	//getting the privacy for a specific window
		{	
			if(view.getRoom()!=null)
			{
				messenger.send("get.privacy", this);
				messenger.send(view.getRoom(), this);
			}
			else
				view.setRoomPrivacy(true);
		}
	}
	
	//controls adding rooms, getting the privacy, and confirming the privacy was changed
	public void response(String reply)
	{
		if(addingRooms)
		{
			if(reply.equals("end.rooms"))
			{
				view.show();
				addingRooms=false;
				update(ControllerMessageType.GETPRIVACY);
			}
			else
				rooms.add(reply);
		}
		else
		{
			if(reply.equals("privacy.changed"))
				view.destroy();
			if(reply.equals("room.public"))
				view.setRoomPrivacy(true);
			if(reply.equals("room.private"))
				view.setRoomPrivacy(false);
		}
	}
	
	//sends request for rooms
	public void start()
	{
		addingRooms=true;
		rooms=new ArrayList<String>();
		messenger.send("get.owner.rooms", this);
		messenger.send(ChatState.getUser().getUserId(), this);
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
	
	//converts the arraylist into a String[]
	public String[] getRooms()
	{
		String[] result=new String[rooms.size()];
		for(int i=0;i<rooms.size();i++)
		{
			result[i]=rooms.get(i);
		}
		return result;
	}
	
	//sorts a given String[]
	public String[] sort(String[] list)
	{
		return main.sort(list);
	}
}
