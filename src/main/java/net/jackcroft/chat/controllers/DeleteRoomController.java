package net.jackcroft.chat.controllers;

import java.util.ArrayList;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.views.DeleteRoomView;

public class DeleteRoomController implements Controller{

	/**
	 * controls the view for deleting a chat room
	 */
	
	private MainController main;
	private Messenger messenger;
	private DeleteRoomView view;
	private String room;
	private ArrayList<String> rooms=new ArrayList<String>();
	private boolean addingRooms=false;
	
	public DeleteRoomController(DeleteRoomView v, Messenger m)
	{
		view=v;
		messenger=m;
		v.setController(this);
	}
	
	public void update(ControllerMessageType type)
	{
		if(type==ControllerMessageType.DELETEROOM)	//deletes a room
		{
			room=view.getRoom();
			if(room!=null)
			{
				messenger.send("delete.room", this);
				messenger.send(room, this);
			}
			else
				view.error("No Room Selected");
		}
		if(type==ControllerMessageType.CANCEL)	//closes the window
			view.destroy();
	}
	
	//controls adding rooms and seeing when a room is deleted
	public void response(String reply)
	{
		if(addingRooms)
		{
			if(reply.equals("end.rooms"))
			{
				view.show();
				addingRooms=false;
			}
			else
				rooms.add(reply);
		}
		else
		{
			if(reply.equals("room.deleted"))
			{
				view.destroy();
				main.update(ControllerMessageType.DELETEROOMDONE);
			}
		}
	}
	
	//sends request for rooms
	public void start()
	{
		addingRooms=true;
		rooms=new ArrayList<String>();
		messenger.send("get.owner.rooms", this);
		messenger.send(ChatState.getUser().getUserId(), this);
	}
	
	//converts the arraylist into a String[]
	public String[] getRooms()
	{
		String[] result=new String[rooms.size()];
		for(int i=0;i<rooms.size();i++)
		{
			result[i]=rooms.get(i);
		}
		return result;
	}
	
	//sorts the given array
	public String[] sort(String[] list)
	{
		return main.sort(list);
	}
	
	//sets the main controller
	public void setMainController(MainController m)
	{
		main=m;
	}
}
