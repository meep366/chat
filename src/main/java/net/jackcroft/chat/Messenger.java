/**
 * 
 */
package net.jackcroft.chat;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import net.jackcroft.chat.controllers.ChatController;
import net.jackcroft.chat.controllers.Controller;
import net.jackcroft.chat.controllers.MainController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class Messenger {

	/**
	 * sends messages to server and handles incoming messages
	 */
	
	private static Logger logger = LoggerFactory.getLogger(Messenger.class);
	private SSLSocket socket=null;
	private String serverName="";
	private int serverPort=4444;
	private boolean rooms;
	private DataOutputStream streamOut=null;
	private ChatClientThread client=null;
	private Controller control=null;
	private ArrayList<ChatController> chatting=new ArrayList<ChatController>();
	private int message=0;
	private String roomName="";
	private String messageText="";
	private String user="";
	
	public Messenger(String name, int port)
	{
		serverName=name;
		serverPort=port;
	}
	
	//sets the name of the server
	public void setName(String name)
	{
		serverName=name;
	}
	
	//connects to the server
	public void connect(MainController main)
	{
		logger.info("Establishing connection. Please wait ...");
		try
		{
			SSLSocketFactory sslsf=(SSLSocketFactory)SSLSocketFactory.getDefault();
			socket=(SSLSocket)sslsf.createSocket(serverName,serverPort);
			String[] suites=sslsf.getSupportedCipherSuites();
			socket.setEnabledCipherSuites(suites);
			logger.info("Connected: "+socket);
			open();
			main.response("connected");
		}
		catch(UnknownHostException uhe)
		{
			logger.error("Host unknown: "+uhe.getMessage());
		}
		catch(IOException ioe)
		{
			logger.error("Unexpected exception: "+ioe.getMessage());
			main.response("no.connection");
		}
	}
	
	//sends a message to the server
	public void send(String msg, Controller c)
	{
		logger.debug("Send Message: "+msg);
		control=c;
		try
		{
			streamOut.writeUTF(msg);
			streamOut.flush();
		}
		catch(IOException ioe)
		{
			logger.error("Sending error: "+ioe.getMessage());
			close();
		}
	}
	
	//handles incoming messages from the server
	public void handle(String msg)
	{
		logger.debug("Incoming Message: "+msg);
		
		if(!rooms&&message==0)
		{
			if(msg.equals("send.message"))
				message=1;
			else if(msg.equals("begin.rooms"))
				rooms=true;
			else if(msg.equals(".bye"))
			{
				logger.info("Closing Chat Client");
				close();
			}
			else
				control.response(msg);

		}
		else if(rooms&&message==0)
		{
			if(msg.equals("end.rooms"))
			{
				control.response(msg);
				rooms=false;
			}
			else
				control.response(msg);
		}
		else
		{
			if(message==1)
			{
				roomName=msg;
				message=2;
			}
			else if(message==2)
			{
				user=msg;
				message=3;
			}
			else if(message==3)
			{
				messageText=msg;
				message=4;
			}
			else
			{
				for(int i=0;i<chatting.size();i++)
				{
					if(chatting.get(i).getChatRoom().equals(roomName))
					{
						chatting.get(i).response("message");
						chatting.get(i).response(user);
						chatting.get(i).response(messageText);
						chatting.get(i).response(msg);
					}
				}
				message=0;
				roomName="";
				user="";
				messageText="";
			}
		}
	}
	
	//opens a new thread
	public void open()
	{
		try
		{
			streamOut=new DataOutputStream(socket.getOutputStream());
			client=new ChatClientThread(this, socket);
		}
		catch(IOException ioe)
		{
			logger.error("Error opening output stream: "+ioe.getMessage());
		}
	}
	
	//closes the thread
	public void close()
	{
		try
		{
			if(socket!=null)
				socket.close();
			if(client!=null)
				client.close();
			if(streamOut!=null)
				streamOut.close();
			
			System.exit(0);
		}
		catch(IOException ioe)	
		{
			logger.error("Error closing ...");
			client.close();
			client.stop();
		}
	}
	
	public void remove(ChatController cc)
	{
		logger.debug("Removing Chat Room From Messenger: "+cc);
		chatting.remove(cc);
	}
	
	//adds a new chat controller
	public void addController(ChatController cc)
	{
		chatting.add(cc);
	}
}
