package net.jackcroft.chat;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import net.jackcroft.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class ChatServer implements Runnable{
	
	/**
	 * reads messages from the client and handles them as well as talks to the database
	 */
	
	private static Logger logger = LoggerFactory.getLogger(ChatServer.class);
	private ChatServerThread[] clients=new ChatServerThread[50];
	private SSLServerSocket server=null;
	private Thread thread=null;
	private int clientCount=0;
	private int login=0;
	private int register=0;
	private int delete=0;
	private int edit=0;
	private int message=0;
	private int deleteRoom=0;
	private int getRooms=0;
	private int getOwnerRooms=0;
	private int addRoom=0;
	private int addUser=0;
	private int removeUser=0;
	private int changePrivacy=0;
	private int getPrivacy=0;
	private int getMessages=0;
	private int getPrivateOwnerRooms=0;
	private String username;
	private String password;
	private String newUser="";
	private String roomName="";
	private String messageText="";
	private boolean isPublic=false;
	private Database db=new Database();
	
	public ChatServer(int port)
	{
		try
		{
			logger.info("Binding to port "+port+", please wait...");
			SSLServerSocketFactory sslssfac=(SSLServerSocketFactory)SSLServerSocketFactory.getDefault();;
			server=(SSLServerSocket)sslssfac.createServerSocket(port);
			String[] suites=server.getEnabledCipherSuites();
			server.setEnabledCipherSuites(suites);
			logger.info("Server started: "+server);
			start();
		}
		catch(IOException ioe)
		{
			logger.error(ioe.getMessage());
		}
	}
	
	//authenticates user login with database
	public boolean validateUser(String user_id,String user_password)
	{
		if(db.findUserPassword(user_id).equals(user_password))
			return true;
		
		return false;
	}
	
	//runs the server while there is a thread active
	public void run()
	{
		while(thread!=null)
		{
			try
			{
				logger.info("Waiting for a client ...");
				addThread((SSLSocket)server.accept());
			}
			catch(IOException ioe)
			{
				logger.error("Server accept error: "+ioe.getMessage());
				stop();
			}
		}
	}
	
	//finds the index in the array of clients for a given client ID
	private int findClient(int ID)
	{
		for(int i=0;i<clientCount;i++)
		{
			if(clients[i].getID()==ID)
				return i;
		}
		return -1;
	}
	
	//sends message out to all users or removes client from server
	public synchronized void handle(int ID, String input)
	{	
			logger.debug("Incoming Message: "+input);
		
			if(input.equals(".bye"))
			{
				clients[findClient(ID)].send(".bye");
				remove(ID);
			}
			if(input.equals("add.user"))
			{
				addUser=1;
				return;
			}
			if(addUser==1)
			{
				username=input;
				addUser=2;
				return;
			}
			if(addUser==2)
			{
				if(db.userExists(username))
				{
					db.addUserToRoom(input, username);
					clients[findClient(ID)].send("user.added");
				}
				else
					clients[findClient(ID)].send("no.such.user");
				
				addUser=0;
				username="";
				return;
			}
			
			if(input.equals("edit"))
			{
				edit=1;
				return;
			}
			if(edit==1)
			{
				username=input;
				edit=2;
				return;
			}
			if(edit==2)
			{
				db.updatePassword(username, input);
				clients[findClient(ID)].send("password.changed");
				edit=0;
				username="";
				return;
			}
			
			if(input.equals("delete"))
			{
				delete=1;
				return;
			}
			
			if(delete==1)
			{
				username=input;
				delete=2;
				return;
			}
			
			if(delete==2)
			{
				password=db.findUserPassword(username);
				
				if(input.equals(password))
				{
					db.deleteUser(username);
					clients[findClient(ID)].send("account.deleted");
				}
				else
					clients[findClient(ID)].send("delete.failed");
				
				delete=0;
				username="";
				password="";
				
				return;
			}
			
			if(input.equals("register"))
			{
				register=1;
				return;
			}
			
			if(register==1)
			{
				newUser=input;
				if(db.userExists(newUser))
				{
					clients[findClient(ID)].send("user.taken");
					return;
				}
				else
				{
					register=2;
					return;
				}
			}
			
			if(register==2)
			{
				db.addUser(newUser, input);
				newUser="";
				clients[findClient(ID)].send("user.added");
				register=0;
				return;
			}
			
			if(input.equals("login"))
			{
				login=1;
				return;
			}
			
			if(login==1)
			{
				username=input;
				login=2;
				return;
			}
			
			if(login==2)
			{
				password=db.findUserPassword(username);
				
				if(input.equals(password))
					clients[findClient(ID)].send("login.accepted");
				else
					clients[findClient(ID)].send("login.failed");
				
				login=0;
				username="";
				password="";
				return;
			}
			
			if(input.equals("get.rooms"))
			{
				getRooms=1;
				return;
			}
			if(getRooms==1)
			{
				username=input;
				ArrayList<String> rooms=db.getRoomsForUser(username);
				clients[findClient(ID)].send("begin.rooms");
				for(int i=0;i<rooms.size();i++)
				{
					clients[findClient(ID)].send(rooms.get(i));
				}
				clients[findClient(ID)].send("end.rooms");
				
				username="";
				getRooms=0;
				return;
			}
			
			if(input.equals("add.room"))
			{
				addRoom=1;
				return;
			}
			
			if(addRoom==1)
			{
				roomName=input;
				addRoom=2;
				return;
			}
			
			if(addRoom==2)
			{
				username=input;
				addRoom=3;
				return;
			}
			if(addRoom==3)
			{
				if(input.equals("true"))
					isPublic=true;
				
				try
				{
					db.createRoom(roomName, isPublic, username);
					clients[findClient(ID)].send("room.added");
				}
				catch(SQLException e)
				{
					if(e.getSQLState().equals("23505"))
						clients[findClient(ID)].send("room.taken");
					else
					{
						logger.error("Adding Room Error: "+e.getMessage());
					}
				}
				
				roomName="";
				username="";
				addRoom=0;
				isPublic=false;
				return;
			}
			
			if(input.equals("message"))
			{
				message=1;
				return;
			}
			if(message==1)
			{
				roomName=input;
				message=2;
				return;
			}
			if(message==2)
			{
				username=input;
				message=3;
				return;
			}
			if(message==3)
			{
				messageText=input;
				message=4;
				return;
			}
			if(message==4)
			{
				for(int i=0;i<clientCount;i++)
				{
					clients[i].send("send.message");
					clients[i].send(roomName);
					clients[i].send(username);
					clients[i].send(messageText);
					clients[i].send(input);
				}
				
				db.addMessage(username, roomName, messageText, input);
				
				message=0;
				roomName="";
				username="";
				messageText="";
				return;
			}
			if(input.equals("get.owner.rooms"))
			{
				getOwnerRooms=1;
				return;
			}
			if(getOwnerRooms==1)
			{
				ArrayList<String> rooms=db.getRoomsForOwner(input);
				
				clients[findClient(ID)].send("begin.rooms");
				for(int i=0;i<rooms.size();i++)
				{
					clients[findClient(ID)].send(rooms.get(i));
				}
				clients[findClient(ID)].send("end.rooms");
				
				getOwnerRooms=0;
				return;
			}
			if(input.equals("get.private.owner.rooms"))
			{
				getPrivateOwnerRooms=1;
				return;
			}
			if(getPrivateOwnerRooms==1)
			{
				ArrayList<String> rooms=db.getPrivateRoomsForOwner(input);
				
				clients[findClient(ID)].send("begin.rooms");
				for(int i=0;i<rooms.size();i++)
				{
					clients[findClient(ID)].send(rooms.get(i));
				}
				clients[findClient(ID)].send("end.rooms");
				
				getPrivateOwnerRooms=0;
				return;
			}
			if(input.equals("remove.user"))
			{
				removeUser=1;
				return;
			}
			if(removeUser==1)
			{
				username=input;
				removeUser=2;
				return;
			}
			if(removeUser==2)
			{
				if(db.userExists(username))
				{
					db.removeUserFromRoom(input, username);
					clients[findClient(ID)].send("user.removed");
				}
				else
					clients[findClient(ID)].send("no.such.user");
				
				removeUser=0;
				username="";
				return;
			}
			if(input.equals("change.privacy"))
			{
				changePrivacy=1;
				return;
			}
			if(changePrivacy==1)
			{
				db.changePrivacy(input);
				clients[findClient(ID)].send("privacy.changed");
				changePrivacy=0;
				return;
			}
			if(input.equals("get.privacy"))
			{
				getPrivacy=1;
				return;
			}
			if(getPrivacy==1)
			{
				if(db.roomIsPublic(input))
					clients[findClient(ID)].send("room.public");
				else
					clients[findClient(ID)].send("room.private");
				
				getPrivacy=0;
				return;
			}
			
			if(input.equals("delete.room"))
			{
				deleteRoom=1;
				return;
			}
			if(deleteRoom==1)
			{
				db.deleteRoom(input);
				clients[findClient(ID)].send("room.deleted");
				deleteRoom=0;
				return;
			}
			if(input.equals("get.messages"))
			{
				getMessages=1;
				return;
			}
			if(getMessages==1)
			{
				roomName=input;
				ArrayList<String>[] messages=db.getMessages(roomName);
				if(messages[0].size()>0)
				{
					clients[findClient(ID)].send("begin.rooms");
					for(int i=0;i<messages.length;i++)
					{
						for(int j=0;j<messages[i].size();j++)
						{
							clients[findClient(ID)].send(messages[i].get(j));
						}
						clients[findClient(ID)].send("end.group");
					}
					clients[findClient(ID)].send("end.rooms");
				}
				else
					clients[findClient(ID)].send("no.messages");
				roomName="";
				getMessages=0;
				return;
			}
		}
	
	//removes given client from server
	public synchronized void remove(int ID)
	{
		int pos=findClient(ID);
		if(pos>=0)
		{
			ChatServerThread toTerminate=clients[pos];
			logger.info("Removing client thread "+ID+" at "+pos);
			if(pos<clientCount-1)
			{
				for(int i=pos+1;i<clientCount;i++)
				{
					clients[i-1]=clients[i];
				}
			}
			clientCount--;
			try
			{
				toTerminate.close();
			}
			catch(IOException ioe)
			{
				logger.error("Error closing thread: "+ioe.getMessage());
				toTerminate.stop();
			}
		}
	}
	
	//adds a new client to the server if there is space
	public void addThread(SSLSocket socket)
	{
		if(clientCount<clients.length)
		{
			logger.info("Client accepted: "+socket);
			clients[clientCount]=new ChatServerThread(this, socket);
			try
			{
				clients[clientCount].open();
				clients[clientCount].start();
				clientCount++;
			}
			catch(IOException ioe)
			{
				logger.error("Error opening thread: "+ioe.getMessage());
			}
		}
		else
			logger.error("Client refused: maximum"+clients.length+" reached.");
	}
	
	//starts the thread
	public void start()
	{
		if(thread==null)
		{
			thread=new Thread(this);
			thread.start();
		}
	}
	
	//stops the thread
	public void stop()
	{
		if(thread!=null)
		{
			thread.stop();
			thread=null;
		}
	}
	
	public static void main(String[] args) 
	{	
		ChatServer server=null;
		if(args.length!=1)
			logger.error("Usage: java ChatServer port");
		else
			server=new ChatServer(Integer.parseInt(args[0]));
	}
}
