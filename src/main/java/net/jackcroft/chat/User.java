package net.jackcroft.chat;

/**
 * 
 */

/**
 * @author jack
 *
 */
public class User {

	/**
	 * represents a user
	 */
	
	private String userId;
	private String password;
	
	public User(String userId, String password)
	{
		this.setUserId(userId);
		this.setPassword(password);
	}
	
	//returns the userId
	public String getUserId() 
	{
		return userId;
	}

	//sets the userId
	public void setUserId(String userId) 
	{
		this.userId = userId;
	}
	
	//returns the password
	public String getPassword() 
	{
		return password;
	}

	//sets the password
	public void setPassword(String password) 
	{
		this.password = password;
	}
}
