package net.jackcroft.chat;

import net.jackcroft.chat.Messenger;
import net.jackcroft.chat.controllers.AddUserController;
import net.jackcroft.chat.controllers.BadOpeningController;
import net.jackcroft.chat.controllers.ChangePrivacyController;
import net.jackcroft.chat.controllers.ChatRoomAddController;
import net.jackcroft.chat.controllers.ChatRoomsController;
import net.jackcroft.chat.controllers.DeleteController;
import net.jackcroft.chat.controllers.DeleteRoomController;
import net.jackcroft.chat.controllers.EditController;
import net.jackcroft.chat.controllers.LoginController;
import net.jackcroft.chat.controllers.MainController;
import net.jackcroft.chat.controllers.RegisterController;
import net.jackcroft.chat.controllers.RemoveUserController;
import net.jackcroft.chat.views.AddUserView;
import net.jackcroft.chat.views.BadOpeningView;
import net.jackcroft.chat.views.ChangePrivacyView;
import net.jackcroft.chat.views.ChatRoomAddView;
import net.jackcroft.chat.views.ChatRoomsView;
import net.jackcroft.chat.views.DeleteRoomView;
import net.jackcroft.chat.views.DeleteView;
import net.jackcroft.chat.views.EditView;
import net.jackcroft.chat.views.LoginView;
import net.jackcroft.chat.views.MainView;
import net.jackcroft.chat.views.RegisterView;
import net.jackcroft.chat.views.RemoveUserView;

public class ChatClient{
	
	/**
	 * 	initiates the whole process
	 */
	
	public static void main(String[] args)
	{
		Messenger m=new Messenger(args[0],Integer.parseInt(args[1]));
		m.setName(args[0]);
		LoginView lv=new LoginView();
		LoginController lc=new LoginController(lv,m);
		RegisterView rv=new RegisterView();
		RegisterController rc=new RegisterController(rv,m);
		DeleteView dv=new DeleteView();
		DeleteController dc=new DeleteController(dv,m);
		EditView ev=new EditView();
		EditController ec=new EditController(ev,m);
		ChatRoomsView crv=new ChatRoomsView();
		ChatRoomsController crc=new ChatRoomsController(crv,m);
		ChatRoomAddView crav=new ChatRoomAddView();
		ChatRoomAddController crac=new ChatRoomAddController(crav,m);
		AddUserView auv=new AddUserView();
		AddUserController auc=new AddUserController(auv,m);
		RemoveUserView ruv=new RemoveUserView();
		RemoveUserController ruc=new RemoveUserController(ruv,m);
		ChangePrivacyView cpv=new ChangePrivacyView();
		ChangePrivacyController cpc=new ChangePrivacyController(cpv,m);
		DeleteRoomView drv=new DeleteRoomView();
		DeleteRoomController drc=new DeleteRoomController(drv,m);
		BadOpeningView bov=new BadOpeningView();
		BadOpeningController boc=new BadOpeningController(bov,m);
		MainView mv=new MainView();
		MainController mc=new MainController(mv, m, lc, rc, dc, ec,crc,crac,auc,ruc,cpc,drc,boc);
		lc.setMainController(mc);
		rc.setMainController(mc);
		dc.setMainController(mc);
		ec.setMainController(mc);
		crc.setMainController(mc);
		crac.setMainController(mc);
		auc.setMainController(mc);
		ruc.setMainController(mc);
		cpc.setMainController(mc);
		drc.setMainController(mc);
		m.connect(mc);
	}	
}