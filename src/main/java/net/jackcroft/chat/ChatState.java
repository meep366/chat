package net.jackcroft.chat;

public final class ChatState {

	/**
	 * Represents the current user
	 */
	
	private static User u;
	
	//sets the current user
	public static void setUser(User user)
	{
		u=user;
	}
	
	//returns the current user
	public static User getUser()
	{
		return u;
	}
}
