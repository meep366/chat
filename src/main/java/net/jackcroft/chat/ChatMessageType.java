package net.jackcroft.chat;

public enum ChatMessageType {	//basic class for holding incoming server messages and keeping them together
	roomTaken, noSuchUser, roomAdded, userRemoved, privacyChanged, roomDeleted, loginAccepted, loginFailed, userAdded, userTaken, accountDeleted, deleteFailed, passwordChanged, roomPublic, roomPrivate;
	
	public static boolean contains(String msg)
	{
		ChatMessageType type=convert(msg);
		
		if(type!=null)
			return true;
		
		return false;
	}
	
	public static ChatMessageType convert(String msg)
	{
		if(msg.equals("room.taken"))
			return roomTaken;
		else if(msg.equals("no.such.user"))
			return noSuchUser;
		else if(msg.equals("room.added"))
			return roomAdded;
		else if(msg.equals("user.removed"))
			return userRemoved;
		else if(msg.equals("privacy.changed"))
			return privacyChanged;
		else if(msg.equals("room.deleted"))
			return roomDeleted;
		else if(msg.equals("login.accepted"))
			return loginAccepted;
		else if(msg.equals("login.failed"))
			return loginFailed;
		else if(msg.equals("user.added"))
			return userAdded;
		else if(msg.equals("user.taken"))
			return userTaken;
		else if(msg.equals("account.deleted"))
			return accountDeleted;
		else if(msg.equals("delete.failed"))
			return deleteFailed;
		else if(msg.equals("password.changed"))
			return passwordChanged;
		else if(msg.equals("room.public"))
			return roomPublic;
		else if(msg.equals("room.private"))
			return roomPrivate;
		
		return null;
	}
}
