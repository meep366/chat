package net.jackcroft.chat;

import java.io.DataInputStream;
import java.io.IOException;

import javax.net.ssl.SSLSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class ChatClientThread extends Thread{

	/**
	 * sends the messages to the server and receives responses back
	 */
	
	private static Logger logger = LoggerFactory.getLogger(ChatClientThread.class);
	private SSLSocket socket=null;
	private Messenger messenger=null;
	private DataInputStream streamIn=null;
	
	public ChatClientThread(Messenger _messenger, SSLSocket _socket)
	{
		messenger=_messenger;
		socket=_socket;
		open();
		start();
	}
	
	//opens a new thread
	public void open()
	{
		try
		{
			streamIn=new DataInputStream(socket.getInputStream());
		}
		catch(IOException ioe)
		{
			logger.error("Error getting input stream: "+ioe.getMessage());
		}
	}
	
	//closes a thread
	public void close()
	{
			try
			{
				if(streamIn!=null)
					streamIn.close();
			}
			catch(IOException ioe)
			{
				logger.error("Error getting input stream: "+ioe.getMessage());
			}
	}
	
	//runs the thread
	public void run()
	{
		while(!socket.isClosed())
		{
			try
			{
				messenger.handle(streamIn.readUTF());
			}
			catch(IOException ioe)
			{
				logger.error("Listening error: "+ioe.getMessage());
			}
		}
	}
}
