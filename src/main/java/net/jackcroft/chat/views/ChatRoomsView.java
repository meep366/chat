package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.table.JTableHeader;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.ChatRoomsController;

public class ChatRoomsView implements View{

	/**
	 * displays the chat rooms view
	 */
	
	private JFrame frame;
	private JTable table=new JTable(30,1);
	private ChatRoomsController controller;
	private DefaultListModel<String> model;
	private long time;
	private String selected;
	private String lastSelected;
	private JList<String> list;
	private JScrollPane pane;
	private Button join;
	private Button addRoom;
	private ArrayList<String> rooms;
	private JMenuBar menu;
	private JMenu file;
	private JMenu chatRoom;
	private JMenu account;
	private JMenuItem refresh;
	private JMenuItem quit;
	private JMenuItem addUser;
	private JMenuItem removeUser;
	private JMenuItem deleteRoom;
	private JMenuItem changePassword;
	private JMenuItem deleteAccount;
	private JMenuItem changePrivacy;
	private JMenuItem logout;
	private JMenuItem createRoom;
	private boolean entered=false;
	
	//displays the window
	public void show()
	{
		join=new Button("Join");
		addRoom=new Button("Create Room");
		menu=new JMenuBar();
		file=new JMenu("File");
		chatRoom=new JMenu("Chat Room");
		account=new JMenu("Account");
		refresh=new JMenuItem("Refresh List");
		quit=new JMenuItem("Quit");
		addUser=new JMenuItem("Add User To Chat Room");
		removeUser=new JMenuItem("Remove User From Chat Room");
		deleteRoom=new JMenuItem("Delete Chat Room");
		changePassword=new JMenuItem("Change Password");
		deleteAccount=new JMenuItem("Delete Account");
		changePrivacy=new JMenuItem("Change Room Privacy");
		logout=new JMenuItem("Logout");
		createRoom=new JMenuItem("Create Room");
		model=new DefaultListModel<String>();
		rooms=new ArrayList<String>();
		list=new JList<String>(model);
		pane=new JScrollPane(list);
		rooms=controller.getRooms();
		frame=new JFrame("Chat Rooms");
		Container contentPane=frame.getContentPane();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		table.setTableHeader(new JTableHeader());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rooms=controller.sort(rooms);

		for(int i=0;i<rooms.size();i++)
		{
			model.addElement(rooms.get(i));
		}
		
		file.add(refresh);
		file.add(logout);
		file.add(quit);
		chatRoom.add(createRoom);
		chatRoom.add(addUser);
		chatRoom.add(removeUser);
		chatRoom.add(changePrivacy);
		chatRoom.add(deleteRoom);
		account.add(changePassword);
		account.add(deleteAccount);
		menu.add(file);
		menu.add(chatRoom);
		menu.add(account);
		frame.add(menu);
		frame.add(pane);
		frame.add(join);
		frame.add(addRoom);
		layout.putConstraint(SpringLayout.WEST,menu,0,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.EAST,menu,0,SpringLayout.EAST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH, addRoom, -10, SpringLayout.SOUTH, contentPane);
		layout.putConstraint(SpringLayout.WEST,addRoom,10,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH, join, -10, SpringLayout.SOUTH, contentPane);
		layout.putConstraint(SpringLayout.WEST,join,10,SpringLayout.EAST,addRoom);
		layout.putConstraint(SpringLayout.NORTH,pane,10,SpringLayout.SOUTH,menu);
		layout.putConstraint(SpringLayout.SOUTH,pane,-40,SpringLayout.SOUTH,contentPane);
		layout.putConstraint(SpringLayout.WEST,pane,10,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.EAST,pane,-10,SpringLayout.EAST,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,400,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,450,SpringLayout.NORTH,contentPane);
		
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		addListeners();
	}
	
	//adds the listeners for the view items
	public void addListeners()
	{
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CLOSE);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		
		list.addMouseListener(new MouseListener()
		{
			
			public void mouseClicked(MouseEvent arg0) {
				
				int y=arg0.getY();
				y=y/18;
				
				if(!(y>=rooms.size()))
				{
					selected=rooms.get(y);
				
					if(timeCheck()&&(selected!=null))
						controller.update(ControllerMessageType.DATAREADY);
				}
			}

			public void mouseEntered(MouseEvent arg0) {
				
			}

			public void mouseExited(MouseEvent arg0) {
				
			}

			public void mousePressed(MouseEvent arg0) {
				
			}

			public void mouseReleased(MouseEvent arg0) {
				
			}
		});
		
		list.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}

			public void keyReleased(KeyEvent arg0) {
				
			}

			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					selected=list.getSelectedValue();
					if(selected!=null)
						controller.update(ControllerMessageType.DATAREADY);
				}
			}
			
		}
		);
		
		join.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				selected=list.getSelectedValue();
				if(selected!=null)
					controller.update(ControllerMessageType.DATAREADY);
			}
			
		});
		
		join.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}

			public void keyReleased(KeyEvent arg0) {
				
			}

			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					selected=list.getSelectedValue();
					if(selected!=null&&!entered)
					{
						entered=true;
						controller.update(ControllerMessageType.DATAREADY);
					}
					
				}
			}
			
		});
		
		addRoom.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.ADDROOM);
			}
			
		});
		
		addRoom.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}

			public void keyReleased(KeyEvent arg0) {
				
			}

			public void keyTyped(KeyEvent arg0) 
			{
				if(arg0.getKeyChar()=='\n')
					controller.update(ControllerMessageType.ADDROOM);
			}
			
		});
		
		refresh.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.refresh();
			}
		});
		
		quit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CLOSE);
			}
		});
		
		logout.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.LOGOUT);
			}
		});
		
		changePassword.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CHANGEPASSWORD);
			}
		});
			
		deleteAccount.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.DELETEACCOUNT);
			}
		});
		
		addUser.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.ADDUSER);
			}
		});
		
		removeUser.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.REMOVEUSER);
			}
		});
		
		changePrivacy.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CHANGEPRIVACY);
			}
		});
		
		deleteRoom.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.DELETEROOM);
			}
		});
		
		createRoom.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.ADDROOM);
			}
		});
		
		
	}
	
	//checks if two clicks are close enough to be a double click
	public boolean timeCheck()
	{
		if(time==0)
		{
			lastSelected=selected;
			time=System.nanoTime();
			return false;
		}
		else
		{
			if(lastSelected==selected)
			{
				float test=(System.nanoTime()-time)/1000000000;
				time=System.nanoTime();
				if(test<1)
				{
					time=System.nanoTime();
					lastSelected=selected;
					return true;
				}
			}
		}
		lastSelected=selected;
		return false;
	}
	
	//sets the entered boolean to false
	public void entered()
	{
		entered=false;
	}
	
	//closes the window
	public void destroy()
	{
		join=null;
		addRoom=null;
		rooms=null;
		menu=null;
		file=null;
		chatRoom=null;
		account=null;
		refresh=null;
		quit=null;
		addUser=null;
		removeUser=null;
		deleteRoom=null;
		changePassword=null;
		deleteAccount=null;
		changePrivacy=null;
		logout=null;
		createRoom=null;
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	//refreshes the list of rooms
	public void refresh()
	{
		rooms=controller.getRooms();
		rooms=controller.sort(rooms);
		for(int i=0;i<rooms.size();i++)
		{
			if(!model.contains(rooms.get(i)))
				model.add(i, rooms.get(i));
		}
		for(int i=0;i<model.getSize();i++)
		{
			if(!rooms.contains(model.get(i)))
				model.remove(i);
		}
	}
	
	//gets the selected room
	public String getSelected()
	{
		return selected;
	}
	
	//sets the controller for the view
	public void setController(ChatRoomsController crc)
	{
		controller=crc;
	}
	
	public static void main(String[] args) 
	{
		ChatRoomsView crv=new ChatRoomsView();
		crv.show();
	}
}
