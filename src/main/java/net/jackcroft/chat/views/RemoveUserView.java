/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.RemoveUserController;

/**
 * @author jack
 *
 */
public class RemoveUserView implements View{

	/**
	 * displays the view for adding a user to a chat room
	 */
	
	private RemoveUserController controller;
	private JFrame frame;
	private Button remove;
	private Button cancel;
	private JLabel error=new JLabel("");
	private JLabel instructions=new JLabel("Please Select A Room");
	private ComboBoxModel<String> model;
	private JComboBox<String> rooms;
	private JTextField userText;
	private JLabel username=new JLabel("Username");
	private String user;
	private String room;
	private String[] roomList;
	
	//displays the window
	public void show()
	{
		remove=new Button("Remove User");
		cancel=new Button("Cancel");
		userText=new JTextField(10);
		roomList=controller.getRooms();
		roomList=controller.sort(roomList);
		frame=new JFrame("Remove User");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane=frame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		model=new DefaultComboBoxModel<String>(roomList);
		rooms=new JComboBox<String>(model);
		frame.add(userText);
		frame.add(username);
		frame.add(instructions);
		frame.add(rooms);
		frame.add(remove);
		frame.add(cancel);
		frame.add(error);
		layout.putConstraint(SpringLayout.NORTH, instructions, 5, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.WEST, instructions, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, rooms, 10, SpringLayout.SOUTH, instructions);
		layout.putConstraint(SpringLayout.WEST, rooms, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, userText, 10, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, userText, 5, SpringLayout.EAST, username);
		layout.putConstraint(SpringLayout.NORTH, username, 10, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, username, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, remove, 15, SpringLayout.SOUTH, username);
		layout.putConstraint(SpringLayout.WEST, remove, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, cancel, 15, SpringLayout.SOUTH, username);
		layout.putConstraint(SpringLayout.WEST, cancel, 5, SpringLayout.EAST, remove);
		layout.putConstraint(SpringLayout.NORTH, error, 15, SpringLayout.SOUTH, remove);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,200,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,200,SpringLayout.WEST,contentPane);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		createListeners();
	}
	
	//creates the listeners for the view items
	public void createListeners()
	{
		remove.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				user=userText.getText();
				controller.update(ControllerMessageType.REMOVEUSER);
			}
			
		});
		
		remove.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
				{
					user=userText.getText();
					controller.update(ControllerMessageType.REMOVEUSER);
				}
			}
			
		});
		
		userText.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				user=userText.getText();
				controller.update(ControllerMessageType.REMOVEUSER);
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}
			
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.CANCEL);
				}
			}
			
		});
		
		rooms.addItemListener(new ItemListener(){

			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange()==ItemEvent.SELECTED)
					room=(String) arg0.getItem();
			}
			
		});
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
	}
	
	//closes the window
	public void destroy()
	{
		error.setText("");
		remove=null;
		cancel=null;
		userText=null;
		rooms=null;
		model=null;
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	//sets the controller for the view
	public void setController(RemoveUserController c)
	{
		controller=c;
	}
	
	//prints an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//returns the user
	public String getUser()
	{
		return user;
	}
	
	//returns the selected room
	public String getRoom()
	{
		if(room==null)
			return (String)rooms.getSelectedItem();
		return room;
	}
	
	public static void main(String[] args) {
		RemoveUserView v=new RemoveUserView();
		v.show();
	}
}
