package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.User;
import net.jackcroft.chat.controllers.DeleteController;
import net.jackcroft.util.swing.SpringUtilities;

public class DeleteView implements View{

	/**
	 * displays the window for deleting a user
	 */
	
	private JFrame deleteFrame;
	private Button delete;
	private Button cancel;
	private JTextField userId;
	private JPasswordField password;
	private DeleteController controller;
	private User user;
	private boolean deleted=false;
	private JLabel error;
	
	//displays the window
	public void show()
	{
		delete=new Button("Delete");
		cancel=new Button("Cancel");
		userId=new JTextField(10);
		password=new JPasswordField(10);
		error=new JLabel("");
		deleteFrame=new JFrame("Delete");
		Container contentPane=deleteFrame.getContentPane();
		deleteFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		contentPane.add(delete);
		contentPane.add(cancel);
		contentPane.add(error);
		layout.putConstraint(SpringLayout.NORTH, error, 10, SpringLayout.SOUTH, delete);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.WEST, cancel, 5, SpringLayout.EAST, delete);
		layout.putConstraint(SpringLayout.SOUTH, cancel, 65, SpringLayout.SOUTH, password);
		layout.putConstraint(SpringLayout.WEST, delete, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH, delete, 65, SpringLayout.SOUTH, password);
		String[] labels={"Username: ", "Password: "};
		int numPairs = labels.length;
		JPanel p = new JPanel(new SpringLayout());
		JLabel userLabel=new JLabel("User ID", JLabel.TRAILING);
		p.add(userLabel);
		userLabel.setLabelFor(userId);
		p.add(userId);
		JLabel passwordLabel=new JLabel("Password", JLabel.TRAILING);
		p.add(passwordLabel);
		passwordLabel.setLabelFor(password);
		p.add(password);
		SpringUtilities.makeCompactGrid(p, numPairs, 2, 6, 6, 6, 6);
		contentPane.add(p);
		layout.putConstraint(SpringLayout.EAST, contentPane, 100, SpringLayout.EAST, userId);
		layout.putConstraint(SpringLayout.SOUTH, contentPane, 100, SpringLayout.SOUTH, userId);
		deleteFrame.setResizable(false);
		deleteFrame.pack();
        deleteFrame.setVisible(true); 
        createListeners();
	}
	
	//creates listeners for the view items
	public void createListeners()
	{
		delete.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				delete();
			}
		});
		
		delete.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					delete();
				}
			}
		});
		
		userId.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				delete();
			}
			
		});
		
		password.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				delete();
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.CANCEL);
				}
			}
		});
		
		deleteFrame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
	}
	
	//prints an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//closes the window
	public void destroy()
	{
		error.setText("");
		deleted=false;
		userId=null;
		password=null;
		if(deleteFrame!=null)
		{
			deleteFrame.dispose();
			deleteFrame=null;
		}
	}
	
	//deletes a user if conditions are met
	public void delete()
	{
		user=new User(userId.getText(),new String(password.getPassword()));
		
		if(!user.getUserId().equals("")&&!user.getPassword().equals(""))
		{
			if(!deleted)
			{
				deleted=true;
				controller.update(ControllerMessageType.DATAREADY);
			}
		}
		else
			error("Empty Field");
	}
	
	//sets the deleted boolean to false
	public void deleted()
	{
		deleted=false;
	}
	
	//sets the controller for the view
	public void setController(DeleteController d)
	{
		controller=d;
	}
	
	//returns the user to be deleted
	public User getUser()
	{
		return user;
	}
	
	public static void main(String[] args) 
	{
		DeleteView dv=new DeleteView();
		dv.show();
	}
}
