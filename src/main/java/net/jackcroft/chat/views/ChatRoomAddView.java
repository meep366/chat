package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.ChatRoomAddController;

public class ChatRoomAddView implements View{

	/**
	 * view for adding a new chat room
	 */
	
	private ChatRoomAddController controller;
	private JFrame frame;
	private JTextField name;
	private JLabel roomName=new JLabel("Chat Room Name");
	private Checkbox hidden;
	private Button add;
	private Button cancel;
	private JLabel error=new JLabel("");
	private String room;
	private boolean added=false;
	
	//displays the window
	public void show()
	{
		frame=new JFrame("Add Room");
		cancel=new Button("Cancel");
		add=new Button("Add");
		name=new JTextField(10);
		hidden=new Checkbox("Hidden");
		Container contentPane=frame.getContentPane();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		frame.add(cancel);
		frame.add(name);
		frame.add(roomName);
		frame.add(hidden);
		frame.add(add);
		frame.add(error);
		layout.putConstraint(SpringLayout.NORTH,error,10,SpringLayout.SOUTH,add);
		layout.putConstraint(SpringLayout.WEST,error,5,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,cancel,15,SpringLayout.SOUTH,hidden);
		layout.putConstraint(SpringLayout.WEST,cancel,5,SpringLayout.EAST,add);
		layout.putConstraint(SpringLayout.WEST,roomName,5,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,name,10,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.NORTH,roomName,10,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.WEST,name,10,SpringLayout.EAST,roomName);
		layout.putConstraint(SpringLayout.NORTH,hidden,10,SpringLayout.SOUTH,name);
		layout.putConstraint(SpringLayout.WEST,hidden,5,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,add,15,SpringLayout.SOUTH,hidden);
		layout.putConstraint(SpringLayout.WEST,add,30,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,250,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,130,SpringLayout.NORTH,contentPane);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		addListeners();
	}
	
	//prints out an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//creates the listeners for the various view objects
	public void addListeners()
	{
		add.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				addRoom();
			}
			
		});
		
		add.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}

			public void keyReleased(KeyEvent arg0) {
				
			}

			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
					addRoom();
				
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
					controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		hidden.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
				{
					if(hidden.getState())
						hidden.setState(false);
					else
						hidden.setState(true);
				}
			}
			
		});
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		
		name.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				addRoom();
			}
		});
	}
	
	//adds a room if the conditions are met
	public void addRoom()
	{
		room=name.getText();
		if(room.length()<=16)
		{
			if(!room.equals(""))
			{
				if(!added)
				{
					added=true;
					controller.update(ControllerMessageType.DATAREADY);
				}
			}	
			else
				error("Empty Field");
		}
		else
			error("Room Name Too Long");
	}
	
	//changes the added boolean to false
	public void added()
	{
		added=false;
	}
	
	//closes the window
	public void destroy()
	{
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
		error.setText("");
		cancel=null;
		name=null;
		add=null;
		hidden=null;
		added=false;
	}
	
	//sets the controller for the view
	public void setController(ChatRoomAddController crac)
	{
		controller=crac;
	}
	
	//returns if the room should be public
	public boolean isPublic()
	{
		return !hidden.getState();
	}
	
	//gets the name of the room
	public String getName()
	{
		return room;
	}
	
	public static void main(String[] args) 
	{
		ChatRoomAddView crav=new ChatRoomAddView();
		crav.show();
	}
}
