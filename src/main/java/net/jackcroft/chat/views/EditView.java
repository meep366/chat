package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ChatState;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.EditController;
import net.jackcroft.util.swing.SpringUtilities;

public class EditView implements View{

	/**
	 * displays the view for changing user password
	 */
	
	
	private JFrame frame;
	private JTextField user_id;
	private JPasswordField oldPassword;
	private JPasswordField conPassword;
	private JPasswordField newPassword;
	private Button confirm;
	private Button cancel;
	private EditController controller;
	private String user;
	private String oldPasswd;
	private String conPasswd;
	private String newPasswd;
	private boolean edited=false;
	private JLabel error;
	
	//displays the view
	public void show()
	{
		oldPassword=new JPasswordField(10);
		conPassword=new JPasswordField(10);
		user_id=new JTextField(10);
		newPassword=new JPasswordField(10);
		confirm=new Button("Confirm");
		cancel=new Button("Cancel");
		error=new JLabel("");
		frame=new JFrame("Change Password");
		Container contentPane=frame.getContentPane();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		String[] labels={"Username: ", "Old Password: ","New Password: ", "Confirm Password: "};
		int numPairs = labels.length;
		JPanel p = new JPanel(new SpringLayout());
		JLabel userLabel=new JLabel("User ID", JLabel.TRAILING);
		p.add(userLabel);
		userLabel.setLabelFor(user_id);
		p.add(user_id);
		JLabel passwordLabel=new JLabel("Old Password", JLabel.TRAILING);
		p.add(passwordLabel);
		passwordLabel.setLabelFor(oldPassword);
		p.add(oldPassword);
		JLabel newPasswordLabel=new JLabel("New Password", JLabel.TRAILING);
		p.add(newPasswordLabel);
		newPasswordLabel.setLabelFor(newPassword);
		p.add(newPassword);
		JLabel conPasswordLabel=new JLabel("Confirm Password", JLabel.TRAILING);
		p.add(conPasswordLabel);
		conPasswordLabel.setLabelFor(conPassword);
		p.add(conPassword);
		SpringUtilities.makeCompactGrid(p, numPairs, 2, 6, 6, 6, 6);
		contentPane.add(p);
		contentPane.add(error);
		contentPane.add(confirm);
		contentPane.add(cancel);
		layout.putConstraint(SpringLayout.NORTH, error, 10, SpringLayout.SOUTH, confirm);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH,cancel,10,SpringLayout.SOUTH,p);
		layout.putConstraint(SpringLayout.WEST,cancel,15,SpringLayout.EAST,confirm);
		layout.putConstraint(SpringLayout.NORTH,confirm,10,SpringLayout.SOUTH,p);
		layout.putConstraint(SpringLayout.WEST,confirm,15,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,315,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,175,SpringLayout.NORTH,contentPane);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		addListeners();
	}
	
	//closes the view
	public void destroy()
	{
		error.setText("");
		user_id=null;
		oldPassword=null;
		newPassword=null;
		conPassword=null;
		confirm=null;
		cancel=null;
		edited=false;
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	//prints an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//checks password matching
	public void change()
	{
		if(newPasswd.equals(conPasswd))
			controller.update(ControllerMessageType.CHANGEPASSWORD);
		else
		{
			error("New Password and Confirm Password do not match");
		}
	}
	
	//sets the edited boolean to false
	public void edited()
	{
		edited=false;
	}
	
	//adds listeners to the view items
	public void addListeners()
	{
		confirm.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				changePassword();
			}
		});
		
		confirm.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					changePassword();
				}
			}
		});
		
		user_id.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				changePassword();
			}
			
		});
		
		oldPassword.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				changePassword();
			}
			
		});
		
		newPassword.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				changePassword();
			}
			
		});
		
		conPassword.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				changePassword();
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.CANCEL);
				}
			}
		});
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		
	}
	
	//changes the password if the conditions are met
	public void changePassword()
	{
		oldPasswd=new String(oldPassword.getPassword());
		conPasswd=new String(conPassword.getPassword());
		newPasswd=new String(newPassword.getPassword());
		user=user_id.getText();
		
		if(!oldPasswd.equals("")&&!conPasswd.equals("")&&!user.equals("")&&!newPasswd.equals(""))
		{
			if(user.equals(ChatState.getUser().getUserId()))
			{
				if(newPasswd.length()<=32)
				{
					if(!edited)
					{
						edited=true;
						controller.update(ControllerMessageType.LOGIN);
					}
				}
				else
					error("New Password too long");
			}
			else
				error("Not signed in as "+user);
		}
		else
			error("Empty Field");
	}
	
	//returns the user
	public String getUser()
	{
		return user;
	}
	
	//returns the old password
	public String getOldPassword()
	{
		return oldPasswd;
	}
	
	//returns the new password
	public String getNewPassword()
	{
		return newPasswd;
	}
	
	//sets the controller for the view
	public void setController(EditController e)
	{
		controller=e;
	}
	
	public static void main(String[] args) 
	{
		EditView ev=new EditView();
		ev.show();
	}
}
