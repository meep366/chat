/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.ChangePrivacyController;

/**
 * @author jack
 *
 */
public class ChangePrivacyView implements View{

	/**
	 * controls the view for changing the privacy of a chat room
	 */
	
	private ChangePrivacyController controller;
	private JFrame frame;
	private Button change;
	private Button cancel;
	private JLabel error=new JLabel("");
	private JLabel instructions=new JLabel("Please Select A Room");
	private ComboBoxModel<String> model;
	private JComboBox<String> rooms;
	private JLabel privacyStatus=new JLabel("Public");
	private JLabel currentPrivacy=new JLabel("Current Privacy:");
	private String room;
	private String[] roomList;
	
	//opens the window
	public void show()
	{
		change=new Button("Change Privacy");
		cancel=new Button("Cancel");
		roomList=controller.getRooms();
		roomList=controller.sort(roomList);
		frame=new JFrame("Change Privacy");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane=frame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		model=new DefaultComboBoxModel<String>(roomList);
		rooms=new JComboBox<String>(model);
		frame.add(privacyStatus);
		frame.add(currentPrivacy);
		frame.add(instructions);
		frame.add(rooms);
		frame.add(change);
		frame.add(cancel);
		frame.add(error);
		layout.putConstraint(SpringLayout.NORTH, instructions, 5, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.WEST, instructions, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, rooms, 10, SpringLayout.SOUTH, instructions);
		layout.putConstraint(SpringLayout.WEST, rooms, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, privacyStatus, 10, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, privacyStatus, 5, SpringLayout.EAST, currentPrivacy);
		layout.putConstraint(SpringLayout.NORTH, currentPrivacy, 10, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, currentPrivacy, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, change, 15, SpringLayout.SOUTH, currentPrivacy);
		layout.putConstraint(SpringLayout.WEST, change, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, cancel, 15, SpringLayout.SOUTH, currentPrivacy);
		layout.putConstraint(SpringLayout.WEST, cancel, 5, SpringLayout.EAST, change);
		layout.putConstraint(SpringLayout.NORTH, error, 15, SpringLayout.SOUTH, change);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,200,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,200,SpringLayout.WEST,contentPane);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		room=(String)rooms.getSelectedItem();
		createListeners();
	}
	
	//adds listeners to the various view items
	public void createListeners()
	{
		change.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CHANGEPRIVACY);
			}
			
		});
		
		change.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
					controller.update(ControllerMessageType.CHANGEPRIVACY);
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}
			
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
					controller.update(ControllerMessageType.CANCEL);
			}
			
		});
		
		rooms.addItemListener(new ItemListener(){

			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange()==ItemEvent.SELECTED)
					room=(String) arg0.getItem();
				controller.update(ControllerMessageType.GETPRIVACY);
			}
			
		});
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
		});
	}
	
	//closes the window
	public void destroy()
	{
		change=null;
		cancel=null;
		rooms=null;
		model=null;
		error.setText("");
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	//sets the controller for the view
	public void setController(ChangePrivacyController c)
	{
		controller=c;
	}
	
	//prints out an error message
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//gets the selected room
	public String getRoom()
	{
		if(room==null)
			return (String)rooms.getSelectedItem();
		return room;
	}
	
	//allows the controller to set the privacy of the current room
	public void setRoomPrivacy(boolean isPublic)
	{
		if(isPublic)
			privacyStatus.setText("Public");
		else
			privacyStatus.setText("Private");
	}
	
	public static void main(String[] args) {
		ChangePrivacyView v=new ChangePrivacyView();
		v.show();
	}

}
