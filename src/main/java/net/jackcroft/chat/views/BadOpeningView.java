/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import net.jackcroft.chat.controllers.BadOpeningController;

/**
 * @author jack
 *
 */
public class BadOpeningView implements View{

	/**
	 * 	view for displaying if there's an opening issue
	 */
	private BadOpeningController controller;
	private JFrame frame;
	private JLabel error;
	private Button close;
	
	public void show()
	{
		frame=new JFrame("Error");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contentPane=frame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		
		error=new JLabel("Server Down, please restart");
		error.setFont(new Font("Helvetica", Font.BOLD, 14));
		close=new Button("Close");
		
		frame.add(error);
		frame.add(close);
		
		layout.putConstraint(SpringLayout.NORTH, close, 10, SpringLayout.SOUTH, error);
		layout.putConstraint(SpringLayout.WEST, close, 85, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, error, 30, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.WEST, error, 20, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH, contentPane, 150, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.EAST, contentPane, 250, SpringLayout.WEST, contentPane);
		
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		addListeners();
	}
	
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	public void addListeners()
	{
		close.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		
		close.addKeyListener(new KeyListener(){

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
					System.exit(0);
			}

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}
			
		});
	}
	
	public void destroy()
	{
		close=null;
		error=null;
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	public void setController(BadOpeningController c)
	{
		controller=c;
	}
	
	public static void main(String[] args) {
		BadOpeningView v=new BadOpeningView();
		v.show();
	}

}
