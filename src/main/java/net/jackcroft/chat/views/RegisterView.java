/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.RegisterController;
import net.jackcroft.util.swing.SpringUtilities;

/**
 * @author jack
 *
 */
public class RegisterView implements View{

	/**
	 * displays the window for registering a new user
	 */
	
	private JFrame registerFrame;
	private JTextField user_id;
	private JPasswordField password;
	private JPasswordField conPassword;
	private Button confirm;
	private Button cancel;
	private RegisterController controller;
	private String user;
	private String passwd;
	private String conPasswd;
	private boolean registered=false;
	private JLabel error=new JLabel("");
	
	//displays the window
	public void show()
	{
		confirm=new Button("Confirm");
		cancel=new Button("Cancel");
		user_id=new JTextField(10);
		password=new JPasswordField(10);
		conPassword=new JPasswordField(10);
		registerFrame=new JFrame("Register");
		Container contentPane=registerFrame.getContentPane();
		registerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		String[] labels={"Username: ", "Password: ", "Confirm Password"};
		int numPairs = labels.length;
		JPanel p = new JPanel(new SpringLayout());
		JLabel userLabel=new JLabel("User ID", JLabel.TRAILING);
		p.add(userLabel);
		userLabel.setLabelFor(user_id);
		p.add(user_id);
		JLabel passwordLabel=new JLabel("Password", JLabel.TRAILING);
		p.add(passwordLabel);
		passwordLabel.setLabelFor(password);
		p.add(password);
		JLabel conPasswordLabel=new JLabel("Confirm Password", JLabel.TRAILING);
		p.add(conPasswordLabel);
		conPasswordLabel.setLabelFor(conPassword);
		p.add(conPassword);
		SpringUtilities.makeCompactGrid(p, numPairs, 2, 6, 6, 6, 6);
		contentPane.add(p);
		contentPane.add(confirm);
		contentPane.add(cancel);
		contentPane.add(error);
		layout.putConstraint(SpringLayout.NORTH,error,15,SpringLayout.SOUTH,confirm);
		layout.putConstraint(SpringLayout.WEST,error,15,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,cancel,15,SpringLayout.SOUTH,p);
		layout.putConstraint(SpringLayout.WEST,cancel,15,SpringLayout.EAST,confirm);
		layout.putConstraint(SpringLayout.NORTH,confirm,15,SpringLayout.SOUTH,p);
		layout.putConstraint(SpringLayout.WEST,confirm,15,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,300,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,175,SpringLayout.NORTH,contentPane);
		registerFrame.setResizable(false);
		registerFrame.pack();
		registerFrame.setVisible(true);
		addListeners();
	}
	
	//prints an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//adds listeners to view items
	public void addListeners()
	{
		confirm.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				register();
			}
		});
		
		confirm.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					register();
				}
			}
		});
		
		user_id.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				register();
			}
			
		});
		
		password.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				register();
			}
			
		});
		
		conPassword.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				register();
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.CANCEL);
				}
			}
		});
		
		registerFrame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
	}
	
	//closes the view
	public void destroy()
	{
		error.setText("");
		registered=false;
		user_id=null;
		password=null;
		conPassword=null;
		confirm=null;
		cancel=null;
		if(registerFrame!=null)
		{
			registerFrame.dispose();
			registerFrame=null;
		}
	}
	
	//registers a new user if conditions are met
	public void register()
	{
		passwd=new String(password.getPassword());
		conPasswd=new String(conPassword.getPassword());
		user=user_id.getText();
		
		if(!user.equals("")&&!passwd.equals("")&&!conPasswd.equals(""))
		{
			if(passwd.equals(conPasswd))
			{
				if(user.length()<=16)
				{
					if(passwd.length()<=32)
					{
						if(!registered)
						{
							registered=true;
							controller.update(ControllerMessageType.DATAREADY);
						}
					}
					else
						error("Password too long");
				}
				else
					error("Username too long");
			}
			else
				error("Password and Confirm Password do not match");
		}
		else
			error("Empty Field");
	}
	
	//sets the registered boolean to false;
	public void registered()
	{
		registered=false;
	}
	
	//returns the password
	public String getPassword()
	{
		return passwd;
	}
	
	//returns the user
	public String getUser()
	{
		return user;
	}
	
	//sets the controller for the view
	public void setController(RegisterController c)
	{
		controller=c;
	}
	
	public static void main(String[] args) 
	{
		RegisterView rv=new RegisterView();
		rv.show();
	}
}
