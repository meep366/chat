/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.DeleteRoomController;

/**
 * @author jack
 *
 */
public class DeleteRoomView implements View{

	/**
	 * displays the view for deleting a chat room
	 */
	
	private DeleteRoomController controller;
	private JFrame frame;
	private Button delete;
	private Button cancel;
	private JLabel error=new JLabel("");
	private JLabel instructions=new JLabel("Please Select A Room");
	private ComboBoxModel<String> model;
	private JComboBox<String> rooms;
	private String room;
	private String[] roomList;
	
	//creates the display
	public void show()
	{
		delete=new Button("Delete Room");
		cancel=new Button("Cancel");
		roomList=controller.getRooms();
		roomList=controller.sort(roomList);
		frame=new JFrame("Delete Room");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane=frame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		model=new DefaultComboBoxModel<String>(roomList);
		rooms=new JComboBox<String>(model);
		frame.add(instructions);
		frame.add(rooms);
		frame.add(delete);
		frame.add(cancel);
		frame.add(error);
		layout.putConstraint(SpringLayout.NORTH, instructions, 5, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.WEST, instructions, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, rooms, 10, SpringLayout.SOUTH, instructions);
		layout.putConstraint(SpringLayout.WEST, rooms, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, delete, 15, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, delete, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, cancel, 15, SpringLayout.SOUTH, rooms);
		layout.putConstraint(SpringLayout.WEST, cancel, 5, SpringLayout.EAST, delete);
		layout.putConstraint(SpringLayout.NORTH, error, 15, SpringLayout.SOUTH, delete);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,150,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.EAST,contentPane,200,SpringLayout.WEST,contentPane);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		room=(String)rooms.getSelectedItem();
		createListeners();
	}
	
	//creates the listeners for the view items
	public void createListeners()
	{
		delete.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.DELETEROOM);
			}
			
		});
		
		delete.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
					controller.update(ControllerMessageType.DELETEROOM);
			}
			
		});
		
		cancel.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}
			
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent e) {
				
			}

			public void keyReleased(KeyEvent e) {
				
			}

			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar()=='\n')
					controller.update(ControllerMessageType.CANCEL);
			}
			
		});
		
		rooms.addItemListener(new ItemListener(){

			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange()==ItemEvent.SELECTED)
					room=(String) arg0.getItem();
			}
			
		});
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
	}
	
	//closes the window
	public void destroy()
	{
		rooms=null;
		model=null;
		delete=null;
		cancel=null;
		error.setText("");
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	//prints an error message on the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//returns the selected room
	public String getRoom()
	{
		if(room==null)
			return (String)rooms.getSelectedItem();
		return room;
	}
	
	//sets the controller for the view
	public void setController(DeleteRoomController c)
	{
		controller=c;
	}
	
	public static void main(String[] args) {
		DeleteRoomView v=new DeleteRoomView();
		v.show();
	}

}
