/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.User;
import net.jackcroft.chat.controllers.LoginController;
import net.jackcroft.util.swing.SpringUtilities;

/**
 * @author jack
 *
 */
public class LoginView implements View{

	/**
	 * displays the view for logging in
	 */
	
	private JFrame loginFrame;
	private Button enter;
	private Button cancel;
	private JTextField userId;
	private JPasswordField password;
	private LoginController controller;
	private User user;
	private boolean loggedIn=false;
	private JLabel error=new JLabel("");
	
	//displays the view
	public void show()
	{
		userId=new JTextField(10);
		password=new JPasswordField(10);
		enter=new Button("Enter");
		cancel=new Button("Cancel");
		loginFrame=new JFrame("Login");
		Container contentPane=loginFrame.getContentPane();
		loginFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		contentPane.add(enter);
		contentPane.add(cancel);
		contentPane.add(error);
		layout.putConstraint(SpringLayout.NORTH, error, 10, SpringLayout.SOUTH, enter);
		layout.putConstraint(SpringLayout.WEST, error, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.WEST, cancel, 5, SpringLayout.EAST, enter);
		layout.putConstraint(SpringLayout.SOUTH, cancel, 65, SpringLayout.SOUTH, password);
		layout.putConstraint(SpringLayout.WEST, enter, 5, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH, enter, 65, SpringLayout.SOUTH, password);
		String[] labels={"Username: ", "Password: "};
		int numPairs = labels.length;
		JPanel p = new JPanel(new SpringLayout());
		JLabel userLabel=new JLabel("User ID", JLabel.TRAILING);
		p.add(userLabel);
		userLabel.setLabelFor(userId);
		p.add(userId);
		JLabel passwordLabel=new JLabel("Password", JLabel.TRAILING);
		p.add(passwordLabel);
		passwordLabel.setLabelFor(password);
		p.add(password);
		SpringUtilities.makeCompactGrid(p, numPairs, 2, 6, 6, 6, 6);
		contentPane.add(p);
		layout.putConstraint(SpringLayout.EAST, contentPane, 100, SpringLayout.EAST, userId);
		layout.putConstraint(SpringLayout.SOUTH, contentPane, 110, SpringLayout.SOUTH, userId);
		loginFrame.setResizable(false);
		loginFrame.pack();
        loginFrame.setVisible(true); 
        createListeners();
	}
	
	//prints an error message to the screen
	public void error(String errorMsg)
	{
		error.setText(errorMsg);
	}
	
	//creates listeners for the view items
	public void createListeners()
	{
		enter.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				login();
			}
		});
		
		enter.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
					login();
			}
		});
		
		cancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		cancel.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
					controller.update(ControllerMessageType.CANCEL);
			}
		});
		
		userId.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				login();
			}
			
		});
		
		password.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				login();
			}
			
		});
		
		loginFrame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}
			
			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.CANCEL);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
	}
	
	//sets the loggedIn boolean to false
	public void loggedIn()
	{
		loggedIn=false;
	}
	
	//logs a user in
	public void login()
	{
		user=new User(userId.getText(),new String(password.getPassword()));
		if(!user.getUserId().equals("")&&!user.getPassword().equals(""))
		{
			if(!loggedIn)
			{
				loggedIn=true;
				controller.update(ControllerMessageType.DATAREADY);
			}
		}
		else
		{
			error("Empty Field");
		}
	}
	
	//closes the window
	public void destroy()
	{
		error.setText("");
		user=null;
		userId=null;
		password=null;
		loggedIn=false;
		enter=null;
		cancel=null;
		if(loginFrame!=null)
		{
			loginFrame.dispose();
			loginFrame=null;
		}
	}
	
	//sets the controller for the view
	public void setController(LoginController lc)
	{
		controller=lc;
	}
	
	//returns the user
	public User getUser()
	{
		return user;
	}
	
	public static void main(String[] args) 
	{
		LoginView lv=new LoginView();
		lv.show();
	}
}
