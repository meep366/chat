package net.jackcroft.chat.views;

public interface View {	//basic interface for all views
	
	public void show();
	public void destroy();

}
