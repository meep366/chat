package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.ChatController;
import org.joda.time.DateTime;

public class ChatView implements View{

	/**
	 * View for displaying a chat room
	 */
	
	private ChatController controller;
	private JFrame frame;
	private JLabel chatRoom;
	private JTable table;
	private DefaultTableModel model;
	private JScrollPane pane;
	private JViewport view;
	private JTextField input;
	private Button quit;
	private Button send;
	private DateTime dt;
	private TableCellRenderer cells;
	private String chatRoomName;
	private String text;
	
	public void show()
	{
		chatRoomName=controller.getChatRoom();
		cells=new LineWrapCellRenderer();
		frame=new JFrame("Chat Room");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane=frame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		input=new JTextField(43);
		quit=new Button("Quit");
		send=new Button("Send");
		chatRoom=new JLabel(chatRoomName, JLabel.CENTER);
		chatRoom.setFont(new Font("Helvetica", Font.BOLD, 14));
		pane=new JScrollPane();
		view=pane.getViewport();
		model=new DefaultTableModel();
		table=new JTable(model);
		table.setDefaultRenderer(Object.class,cells);
		model.addColumn("One");
		model.addColumn("Two");
		model.addColumn("Three");
		table.setTableHeader(null);
		table.setShowGrid(false);
		table.setIntercellSpacing(new Dimension(0,0));
		view.setBackground(Color.WHITE);
		view.add(table);
		frame.add(pane);
		frame.add(chatRoom);
		frame.add(input);
		frame.add(quit);
		frame.add(send);
		layout.putConstraint(SpringLayout.NORTH, quit, -30, SpringLayout.SOUTH, contentPane);
		layout.putConstraint(SpringLayout.NORTH, send, -30, SpringLayout.SOUTH, contentPane);
		layout.putConstraint(SpringLayout.WEST,quit,10,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.WEST,send,5,SpringLayout.EAST,input);
		layout.putConstraint(SpringLayout.EAST, send, -10, SpringLayout.EAST, contentPane);
		layout.putConstraint(SpringLayout.NORTH, input, -25, SpringLayout.SOUTH, contentPane);
		layout.putConstraint(SpringLayout.WEST, input, 5, SpringLayout.EAST, quit);
		layout.putConstraint(SpringLayout.NORTH,pane,10,SpringLayout.SOUTH,chatRoom);
		layout.putConstraint(SpringLayout.EAST, pane, -10, SpringLayout.EAST, contentPane);
		layout.putConstraint(SpringLayout.WEST, pane, 10, SpringLayout.WEST, contentPane);
		layout.putConstraint(SpringLayout.SOUTH, pane, -10, SpringLayout.NORTH, input);
		layout.putConstraint(SpringLayout.EAST,chatRoom,0,SpringLayout.EAST,contentPane);
		layout.putConstraint(SpringLayout.WEST,chatRoom,0,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,chatRoom,0,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,chatRoom,20,SpringLayout.NORTH,contentPane);
		layout.putConstraint(SpringLayout.SOUTH, contentPane, 600, SpringLayout.NORTH, contentPane);
		layout.putConstraint(SpringLayout.EAST, contentPane, 600, SpringLayout.WEST, contentPane);
		table.setEnabled(false);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
		addListeners();
	}
	
	public class LineWrapCellRenderer extends JTextArea implements TableCellRenderer {
		private static final long serialVersionUID = 1L;
		private int rowHeight;  // current max row height for this scan
		private int nameColWidth;
		private int timeColWidth;
		private int messageColWidth;
		
		public Component getTableCellRendererComponent(
		        JTable table,
		        Object value,
		        boolean isSelected,
		        boolean hasFocus,
		        int row,
		        int column)
		{
		    setText((String) value);
		    setWrapStyleWord(true);
		    setLineWrap(true);
		    
		    // current table column width in pixels
	//	    int defaultWidth = table.getColumnModel().getColumn(column).getWidth();
		    int specificWidth=0;
		    
		    if(column==2)
		    {
		    	FontMetrics fm=getFontMetrics(getFont());
		    	specificWidth=fm.stringWidth(getText())+5;
		    	
		    	if(specificWidth>timeColWidth)
			    {
			    	timeColWidth=specificWidth;
			    	setSize(new Dimension(specificWidth, 1));
			    	table.getColumnModel().getColumn(2).setPreferredWidth(specificWidth);
			    }
		    }
		    
		    else if(column==0)
		    {	
		    	FontMetrics fm=getFontMetrics(getFont());
		    	specificWidth=fm.stringWidth(getText())+5;
		    	
		    	if(specificWidth>nameColWidth)
			    {
			    	nameColWidth=specificWidth;
			    	setSize(new Dimension(specificWidth, 1));
			    	table.getColumnModel().getColumn(0).setPreferredWidth(specificWidth);
			    }
		    }
		    else
		    	setSize(new Dimension(messageColWidth, 1));
		    
		    messageColWidth=577-nameColWidth-timeColWidth;
		    table.getColumnModel().getColumn(1).setPreferredWidth(messageColWidth);
		    
		    		    
		    // get the text area preferred height and add the row margin
		    int height = getPreferredSize().height + table.getRowMargin();
		    // ensure the row height fits the cell with most lines
		    if (column == 0 || height > rowHeight) {
		    	table.setRowHeight(row, height);
		        rowHeight = height;
		    }
		    
		    return this;
		}
	}
	
	public void addListeners()
	{
		input.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				dt=new DateTime();
				text=input.getText();
				controller.update(ControllerMessageType.SEND);
				input.setText("");
			}
		});
		quit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.QUIT);
			}
		});
		send.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				dt=new DateTime();
				text=input.getText();
				controller.update(ControllerMessageType.SEND);
				input.setText("");
			}
		});
		
		send.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					dt=new DateTime();
					text=input.getText();
					controller.update(ControllerMessageType.SEND);
					input.setText("");
				}
			}
		});
		
		quit.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
					controller.update(ControllerMessageType.QUIT);
			}
		});
		
		
		frame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				controller.update(ControllerMessageType.QUIT);
			}

			public void windowClosing(WindowEvent arg0) {
				
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
		});
	}
	
	public void destroy()
	{
		dt=null;
		chatRoom=null;
		table=null;
		model=null;
		pane=null;
		view=null;
		input=null;
		quit=null;
		send=null;
		cells=null;
		if(frame!=null)
		{
			frame.dispose();
			frame=null;
		}
	}
	
	public void print(String time,String user,String message)
	{
		String[] row={user,message,time};
		model.addRow(row);
	}
	
	public void setController(ChatController c)
	{
		controller=c;
	}
	
	public String getText()
	{
		return text;
	}
	
	public DateTime getTime()
	{
		return dt;
	}
	
	public static void main(String[] args) {
		
		ChatView c=new ChatView();
		c.show();
	}
}