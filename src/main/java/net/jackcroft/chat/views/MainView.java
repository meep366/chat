/**
 * 
 */
package net.jackcroft.chat.views;

import java.awt.Button;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import net.jackcroft.chat.ControllerMessageType;
import net.jackcroft.chat.controllers.MainController;

/**
 * @author jack
 *
 */
public class MainView implements View{

	/**
	 *	main opening window that pops up when program starts
	 */
	
	private JFrame mainFrame;
	private Button register;
	private Button login;
	private JLabel welcome=new JLabel("Welcome!");
	private JLabel welcome2=new JLabel("Please Login or Register");
	private MainController controller;
	
	//displays the window
	public void show()
	{
		register=new Button("Register");
		login=new Button("Login");
		mainFrame=new JFrame("Welcome");
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane=mainFrame.getContentPane();
		SpringLayout layout=new SpringLayout();
		contentPane.setLayout(layout);
		mainFrame.add(register);
		mainFrame.add(login);
		mainFrame.add(welcome);
		mainFrame.add(welcome2);
		mainFrame.setResizable(false);
		layout.putConstraint(SpringLayout.NORTH,welcome2,10,SpringLayout.SOUTH, welcome);
		layout.putConstraint(SpringLayout.WEST,welcome,75,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.WEST,welcome2,35,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.WEST,register,30,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.NORTH,login,15,SpringLayout.SOUTH,welcome2);
		layout.putConstraint(SpringLayout.NORTH,register,15,SpringLayout.SOUTH,welcome2);
		layout.putConstraint(SpringLayout.WEST,login,10,SpringLayout.EAST,register);
		layout.putConstraint(SpringLayout.EAST,contentPane,200,SpringLayout.WEST,contentPane);
		layout.putConstraint(SpringLayout.SOUTH,contentPane,125,SpringLayout.NORTH,contentPane);
		mainFrame.pack();
		mainFrame.setVisible(true);
		createListeners();
	}
	
	//closes the window
	public void destroy()
	{
		register=null;
		login=null;
		if(mainFrame!=null)
		{
			mainFrame.dispose();
			mainFrame=null;
		}
	}
	
	//creates listeners for view items
	public void createListeners()
	{
		mainFrame.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent arg0) {
				
			}

			public void windowClosed(WindowEvent arg0) {
				
			}

			public void windowClosing(WindowEvent arg0) {
				controller.update(ControllerMessageType.CLOSE);
			}

			public void windowDeactivated(WindowEvent arg0) {
				
			}

			public void windowDeiconified(WindowEvent arg0) {
				
			}

			public void windowIconified(WindowEvent arg0) {
				
			}

			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		
		login.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.LOGIN);
			}
		});
		
		login.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent arg0) {
				
			}

			public void keyReleased(KeyEvent arg0) {
				
			}

			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.LOGIN);
				}
			}
		});
		
		register.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				controller.update(ControllerMessageType.REGISTER);
			}
		});
		
		register.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {
				
			}
			public void keyTyped(KeyEvent arg0) {
				if(arg0.getKeyChar()=='\n')
				{
					controller.update(ControllerMessageType.REGISTER);
				}
			}
			
		});
	}
	
	//sets the controller for the view
	public void setController(MainController mc)
	{
		controller=mc;
	}
	
	//disables buttons
	public void disableButtons()
	{
		login.setEnabled(false);
		register.setEnabled(false);
	}
	
	//enables buttons
	public void enableButtons()
	{
		login.setEnabled(true);
		register.setEnabled(true);
	}
	
	public static void main(String[] args) 
	{
		MainView mv=new MainView();
		mv.show();
	}
}
