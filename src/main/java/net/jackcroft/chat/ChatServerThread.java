package net.jackcroft.chat;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.net.ssl.SSLSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author jack
 *
 */
public class ChatServerThread extends Thread{

	/**
	 * receives messages from client and sends messages back
	 */
	
	private static Logger logger = LoggerFactory.getLogger(ChatServerThread.class);
	private ChatServer server=null;
	private SSLSocket socket=null;
	private int ID=-1;
	private DataInputStream streamIn=null;
	private DataOutputStream streamOut=null;
	
	public ChatServerThread(ChatServer _server, SSLSocket _socket)
	{
		server=_server;
		socket=_socket;
		ID=socket.getPort();
	}
	
	//sends a message out
	public void send(String msg)
	{
		try
		{
			logger.debug("Sending: "+msg);
			streamOut.writeUTF(msg);
			streamOut.flush();
		}
		catch(IOException ioe)
		{
			logger.error(ID+" ERROR sending: "+ioe.getMessage());
			server.remove(ID);
			stop();
		}
	}
	
	//returns ID of client
	public int getID()
	{
		return ID;
	}
	
	//runs the thread
	public void run()
	{
		logger.info("Server Thread "+ID+" running.");
		while(!socket.isClosed())
		{
			try
			{
				server.handle(ID, streamIn.readUTF());
			}
			catch(IOException ioe)
			{
				logger.error(ID+" ERROR reading: "+ioe.getMessage());
				server.remove(ID);
				stop();
			}
		}
	}
	
	//opens the thread
	public void open() throws IOException
	{
		streamIn=new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		streamOut=new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}
	
	//closes the thread
	public void close() throws IOException
	{
		if(socket!=null)
			socket.close();
		if(streamIn!=null)
			streamIn.close();
		if(streamOut!=null)
			streamOut.close();
	}
}
