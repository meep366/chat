package net.jackcroft.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jack
 *
 */
public class Database {

	/**
	 * does all communication with the database
	 */
	
	private static Logger logger = LoggerFactory.getLogger(Database.class);
	private Connection connection=null;
	
	public Database()
	{
		connect();
	}
	
	//connects to database
	public void connect()
	{
		logger.info("-------PostgreSQL "+"JDBC Connection Testing -----------");
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch(ClassNotFoundException e)
		{
			logger.error("Where is your PostgreSQL JDBC Driver?"+"Include in your library path!");
			logger.error(e.getMessage());
			return;
		}
		
		logger.info("PostgreSQL JDBC Driver Registered!");
		
		try
		{
			connection=DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/chat", "postgres","jackiscool");
		}
		catch(SQLException e)
		{
			logger.error("Connection Failed! Check output console");
			logger.error(e.getMessage());
			return;
		}
	}
	
	public void close()
	{
		logger.debug("Closing Connection");
		try 
		{
			connection.close();
		} 
		catch (SQLException e) 
		{
			logger.error("Closing Error: "+e.getMessage());
		}
	}
	
	//returns whether or not a given user exists
	public boolean userExists(String user)
	{
		logger.debug("Checking If User Exists");
		ResultSet r;
		try 
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select user_id from users where user_id='"+user+"'");
			if(r.next())
				return true;
		} 
		catch (SQLException e) 
		{
			logger.error("User Exists Error: "+e.getMessage());
		}
		
		return false;
	}
	
	//adds new user to Database
	public void addUser(String user, String password)
	{
		logger.debug("Adding User To Database");
		try
		{
			Statement s=connection.createStatement();
			s.execute("insert into users values('"+user+"','"+password+"')");
		}
		catch (SQLException e) 
		{
			logger.error("Add User Error: "+e.getMessage());
		}
	}
	
	//returns the password from database for given user_id
	public String findUserPassword(String user_id)
	{
		logger.debug("Finding User Password");
		String password="";
		ResultSet r;
		try
		{
			Statement s=connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			r=s.executeQuery("select user_id, user_password from users where user_id='"+user_id+"'");
			
			if(r.next())
				password=r.getString("user_password");
				
		}
		catch (SQLException e) 
		{
			logger.error("Find User Password Error: "+e.getMessage());
		}
		
		return password;
	}
	
	//removes user from database
	public void deleteUser(String user_id)
	{
		logger.debug("Deleting User From Database");
		try
		{
			Statement s=connection.createStatement();
			removeRooms(user_id);
			s.execute("delete from user_rooms where user_id='"+user_id+"'");
			s.execute("delete from users where user_id='"+user_id+"'");
		}
		catch (SQLException e) 
		{
			logger.error("Delete User Error: "+e.getMessage());
		}
	}

	//removes all rooms that an owner owns
	public void removeRooms(String user_id)
	{
		logger.debug("Removing Rooms For User");
		ResultSet r;
		ArrayList<String> rooms=new ArrayList<String>();
		try
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select room_id from user_rooms where user_id='"+user_id+"' and permissions='owner'");
			
			while(r.next())
			{
				rooms.add(r.getString("room_id"));
			}
			
			for(int i=0;i<rooms.size();i++)
			{
				connection.setAutoCommit(false);
				s.execute("delete from user_rooms where room_id='"+rooms.get(i)+"'");
				s.execute("delete from chat_rooms where room_id='"+rooms.get(i)+"'");
				connection.commit();
				connection.setAutoCommit(true);
			}
		}
		catch(SQLException e)
		{
			try
			{
				connection.setAutoCommit(true);
				connection.rollback();
			}
			catch(SQLException sqle)
			{
				logger.error("Rollback Error: "+sqle.getMessage());
			}
			logger.error("Remove Rooms Error: "+e.getMessage());
		}
	}
	
	//updates the password of given user in database
	public void updatePassword(String user_id, String password)
	{
		logger.debug("Changing User Password");
		try
		{
			Statement s=connection.createStatement();
			
			s.execute("update users set user_password='"+password+"' where user_id='"+user_id+"'");
		}
		catch (SQLException e) 
		{
			logger.error("Update Password Error: "+e.getMessage());
		}
	}
	
	//creates a new room in the database
	public void createRoom(String room_id, boolean open, String user_id) throws SQLException
	{
		logger.debug("Creating A New Chat Room");
		try
		{
			Statement s=connection.createStatement();
			
			s.execute("insert into chat_rooms values ('"+room_id+"','"+open+"')");
			s.execute("insert into user_rooms values ('"+room_id+"','"+user_id+"', 'owner')");
		}
		catch (SQLException e) 
		{
			throw(e);
		}
	}
	
	//returns whether or not a given room exists
	public boolean roomExists(String room)
	{
		logger.debug("Checking If A Room Exists");
		ResultSet r;
		try
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select room_id from chat_rooms where room_id='"+room+"'");
			
			if(r.next())
				return true;
		}
		catch (SQLException e) 
		{
			logger.error("Room Exists Error: "+e.getMessage());
		}
		return false;
	}
	
	//returns whether or not a given room is public
	public boolean roomIsPublic(String room)
	{
		logger.debug("Checking If A Room Is Public");
		ResultSet r;
		try
		{
			Statement s=connection.createStatement();
			
			r=s.executeQuery("select room_id, public from chat_rooms where room_id='"+room+"'");
			r.next();
			return r.getBoolean("public");
		}
		catch (SQLException e) 
		{
			logger.error("Room Is Public Error: "+e.getMessage());
		}
		return false;
	}
	
	//removes the room from the database
	public void deleteRoom(String room)
	{
		logger.debug("Deleting Chat Room");
		try
		{
			Statement s=connection.createStatement();
			s.execute("delete from messages where room_id='"+room+"'");
			connection.setAutoCommit(false);
			s.execute("delete from user_rooms where room_id='"+room+"'");
			s.execute("delete from chat_rooms where room_id='"+room+"'");
			connection.commit();
			connection.setAutoCommit(true);
		}
		catch (SQLException e) 
		{
			try
			{
				connection.rollback();
				connection.setAutoCommit(true);
			}
			catch(SQLException sqle)
			{
				logger.error("Rollback error: "+e.getMessage());
			}
			logger.error("Delete Room Error: "+e.getMessage());
		}
	}
	
	//adds a new user to the given room
	public void addUserToRoom(String room, String user)
	{
		logger.debug("Adding User To Chat Room");
		try
		{
			Statement s=connection.createStatement();
			ResultSet r;
			r=s.executeQuery("select user_id, room_id from user_rooms where user_id='"+user+"' and room_id='"+room+"'");
			
			if(r.next())
				return;
			
			s.execute("insert into user_rooms values ('"+room+"','"+user+"','guest')");
		}
		catch (SQLException e) 
		{
			logger.error("Add User To Room Error: "+e.getMessage());
		}
	}
	
	//removes the user from the given room
	public void removeUserFromRoom(String room, String user)
	{
		logger.debug("Removing User From Chat Room");
		try
		{
			Statement s=connection.createStatement();
			s.execute("delete from user_rooms where user_id='"+user+"' and room_id='"+room+"'");
		}
		catch (SQLException e) 
		{
			logger.error("Remove User From Room Error: "+e.getMessage());
		}
	}
	
	//gets a list of all rooms a user can see to chat in
	public ArrayList<String> getRoomsForUser(String user)
	{
		logger.debug("Getting Rooms For User");
		ResultSet r;
		ArrayList<String> result=new ArrayList<String>();
		try
		{
			Statement s=connection.createStatement();
			
			r=s.executeQuery("select room_id, public from chat_rooms");
			
			while(r.next())
			{
				if(r.getBoolean("public"))
					result.add(r.getString("room_id"));
			}
			
			r=s.executeQuery("select room_id from user_rooms where user_id='"+user+"'");
			
			while(r.next())
			{
				if(!result.contains(r.getString("room_id")))
					result.add(r.getString("room_id"));
			}
			
		}
		catch (SQLException e) 
		{
			logger.error("Get Rooms For User Error: "+e.getMessage());
		}
		return result;
	}
	
	//changes the privacy of a chat room
	public void changePrivacy(String room)
	{	
		logger.debug("Changing Privacy Of Chat Room");
		try
		{
			Statement s=connection.createStatement();
			
			if(roomIsPublic(room))
				s.execute("update chat_rooms set public='false' where room_id='"+room+"'");
			else
			{
				connection.setAutoCommit(false);
				s.execute("update chat_rooms set public='true' where room_id='"+room+"'");
				s.execute("delete from user_rooms where room_id='"+room+"' and permissions='guest'");
				connection.commit();
				connection.setAutoCommit(true);
			}
		}
		catch (SQLException e) 
		{
			try
			{
				connection.setAutoCommit(true);
				connection.rollback();
			}
			catch(SQLException sqle)
			{
				logger.error("Rollback Error: "+sqle.getMessage());
			}
			logger.error("Change Privacy Error: "+e.getMessage());
		}
		
	}
	
	//gets a list of all the private rooms a given user owns
	public ArrayList<String> getPrivateRoomsForOwner(String user)
	{
		logger.debug("Getting Private Rooms For Owner");
		ResultSet r;
		ArrayList<String> result=new ArrayList<String>();
		
		try
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select room_id from user_rooms where user_id='"+user+"' and permissions='owner'");
			while(r.next())
			{
				if(!roomIsPublic(r.getString("room_id")))
					result.add(r.getString("room_id"));
			}
		}
		catch(SQLException e)
		{
			logger.error("Get Private Rooms For Owner Error: "+e.getMessage());
		}
		
		return result;
	}
	
	//gets a list of all the rooms a given user owns
	public ArrayList<String> getRoomsForOwner(String user)
	{
		logger.debug("Getting Rooms For Owner");
		ResultSet r;
		ArrayList<String> result=new ArrayList<String>();
		
		try
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select room_id from user_rooms where user_id='"+user+"' and permissions='owner'");
			while(r.next())
			{
				result.add(r.getString("room_id"));
			}
		}
		catch(SQLException e)
		{
			logger.error("Get Rooms For Owner Error: "+e.getMessage());
		}
		
		return result;
	}
	
	public void addMessage(String user_id, String room_id, String message, String time)
	{
		logger.debug("Adding Message To Database");
		try
		{
			Statement s=connection.createStatement();
			s.execute("insert into messages values ('"+room_id+"', '"+user_id+"', '"+time+"', '"+message+"')");
		}
		catch(SQLException e)
		{
			logger.error("Adding Message Error: "+e.getMessage());
		}
	}
	
	public ArrayList<String>[] getMessages(String room)
	{
		logger.debug("Getting Messages From Database");
		ArrayList<String>[] result= new ArrayList[3];
		ResultSet r;
		for(int i=0;i<result.length;i++)
		{
			result[i]=new ArrayList<String>();
		}
		
		try
		{
			Statement s=connection.createStatement();
			r=s.executeQuery("select * from messages where room_id='"+room +"' order by time");
			while(r.next())
			{
				result[0].add(r.getString("user_id"));
				result[1].add(r.getString("message"));
				result[2].add(r.getString("time"));
			}
		}
		catch(SQLException e)
		{
			logger.error("Getting Messages Error: "+e.getMessage());
		}
		
		return result;
	}
	
}
