/**
 * 
 */
package net.jackcroft.util;

/**
 * @author jack
 *
 */
public class BaseEncoding {

	/**
	 * converts a given byte[] to an equivalent hex value
	 */
	
	private final static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	public static String convertToHex(byte[] bytes)
	{
		char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for(int j=0; j<bytes.length; j++ ) 
	    {
	    	v = bytes[j] & 0xFF;
	    	hexChars[j * 2] = hexArray[v >>> 4];
		    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
		return new String(hexChars);
	}
}
