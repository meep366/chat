package net.jackcroft.util.ssl;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * 
 */

/**
 * @author jack
 *
 */
public class EchoClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		try
		{
		SSLSocketFactory sslsf=(SSLSocketFactory)SSLSocketFactory.getDefault();
		SSLSocket ssls=(SSLSocket)sslsf.createSocket("localhost",9999);
		InputStream input=System.in;
		InputStreamReader inputReader=new InputStreamReader(input);
		BufferedReader br=new BufferedReader(inputReader);
		OutputStream output=ssls.getOutputStream();
		OutputStreamWriter outputWriter=new OutputStreamWriter(output);
		BufferedWriter bw=new BufferedWriter(outputWriter);
		String s=null;
		while((s=br.readLine())!=null)
		{
			bw.write(s+'\n');
			bw.flush();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

}
