package net.jackcroft.util.ssl;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 * 
 */

/**
 * @author jack
 *
 */
public class EchoServer {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) 
	{
		try
		{
			SSLServerSocketFactory sslssfac=(SSLServerSocketFactory)SSLServerSocketFactory.getDefault();;
			SSLServerSocket sslss=(SSLServerSocket)sslssfac.createServerSocket(9999);
			SSLSocket ssls=(SSLSocket)sslss.accept();
			InputStream input=ssls.getInputStream();
			InputStreamReader inputReader=new InputStreamReader(input);
			BufferedReader br=new BufferedReader(inputReader);
			String s=null;
			while((s=br.readLine())!=null)
			{
				System.out.println(s);
				System.out.flush();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}

}
